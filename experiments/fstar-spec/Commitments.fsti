(* The MIT License (MIT)

 Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. *)

module Commitments

type int63 = Int63.t

// Axiomatic model of commitments, based on [commitments.v] from
// ssr_trees.

// Types for commitments and hashes.
assume type t_C : eqtype
assume type t_H : eqtype

let lit = Int63.int_to_t

val max_height : (h:int63{Int63.(h =^ lit 32)})

type v_height =  h:int63{Int63.(h >=^ lit 0 /\ h <=^ max_height)}

// Default uncommitted hashes by height.
val uncommitted : h:v_height -> t_H

// Hash function to compute Merkle trees.
val merkle_hash : h:v_height -> t_H -> t_H -> t_H

// The hash function is assumed injective at every level.
val perfect_merkle : h:v_height ->
  Lemma (ensures (forall h1 h2 h3 h4.
          merkle_hash h h1 h2 = merkle_hash h h3 h4
            ==> h1 = h3 /\ h2 = h4))
  [SMTPat (merkle_hash h)]

// Hashes and commitments are different names for the same object.
val hash_of_commitment : t_C -> t_H
val hash_to_commitment : t_H -> t_C

// The previous functions determine an isomorphism between c and h.
val hash_to_of_iso0 : hash:t_H ->
  Lemma (ensures  hash_of_commitment (hash_to_commitment hash) = hash)
  [SMTPat (hash_of_commitment (hash_to_commitment hash))]

val hash_to_of_iso1 : comm:t_C ->
  Lemma (ensures  hash_to_commitment (hash_of_commitment comm) = comm)
  [SMTPat (hash_to_commitment (hash_of_commitment comm))]

// We assume the default values given by [uncommitted] satisfy the specification.
val uncommitted_spec : h:int63{Int63.(h >=^ lit 0 /\ h <^ max_height)} ->
  Lemma (ensures ( uncommitted Int63.(h +^ lit 1) =
      merkle_hash h (uncommitted h) (uncommitted h)))
