(* The MIT License (MIT)

 Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. *)

module Tree_insert_identities

open Tree
module C = Commitments
open FStar.List.Tot
module U32 = FStar.UInt32

// The [Tree] module defines 3 insertion functions:
// - insert_model: a naive implementation that checks if the left sub-tree is
//                 full to determine the path.
// - insert_pow: an optimization carrying the next available position.
// - insert_list: a further optimization for batch insertion.

// This module's goal is to prove all 3 equivalent. To do so, [lemma_model_pow]
// proves that [insert_model] is equivalent to [insert_pow], showing that the
// first optimization is safe.
// The second important lemma is embedded into [rec_inserts]' post-condition,
// which states that applying [insert_list] over a list [l] and an IMT [t] is
// equivalent to individually insertiong [l]'s elements with [insert_pow] to [t].

// Same as [count_leaves], but returning an uint32.
val count_leaves' :
  h:C.v_height ->
  t:imt h{not (full t) \/ count_leaves t < pow2 (v63 h) \/ count_leaves t < pow2 32} ->
  Pure uint32
       (requires True)
       (ensures (fun s -> U32.v s = count_leaves t))
let count_leaves' h t =
  let c = count_leaves t in
  max_leaves h t;
  max_leaves_full h t;
  assert((v63 h) <= (v63 C.max_height));
  lem_max_size ();
  FStar.Math.Lemmas.pow2_le_compat (v63 C.max_height) (v63 h);
  U32.uint_to_t c

// [insert_model] is equivalent to [insert_pow].
val lemma_model_pow :
  h:C.v_height ->
  c:C.t_C ->
  t:imt h{not (full t)} ->
  Lemma (requires (count_leaves t < pow2 (v63 C.max_height)))
        (ensures (insert_model c h t = insert_pow c (count_leaves' h t) h t))
        (decreases %[v63 h])
#push-options "--z3rlimit 20"
let rec lemma_model_pow h c t =
  match (t, Int63.(h =^ lit 0)) with
  | (Empty, true) -> ()
  | (Empty, _) -> lemma_model_pow (pred63 h) c Empty
  | (Node _ l r, _) ->
    max_leaves_full (pred63 h) l;
    if full l
    then begin
      lemma_incremental_full_left h t;
      lemma_model_pow (pred63 h) c r
    end
    else lemma_model_pow (pred63 h) c l
#pop-options


// Same as [insert_pow], but expressing the [not (full t)] precondition
// in terms of [count_leaves].
// Used for the following lemmas.
val insert_pow' :
  v:C.t_C ->
  pos:uint32 ->
  h:C.v_height ->
  t:tree C.t_C C.t_H {incremental h t  /\ merkle h t
                      /\ U32.v pos = count_leaves t
		      /\ U32.v pos < pow2 (v63 h)} ->
  Tot (t':tree C.t_C C.t_H {incremental h t' /\ merkle h t'
                            /\ count_leaves t' = count_leaves t + 1})
let insert_pow' v pos h t = max_leaves_full h t; insert_pow v pos h t


// Special case: inserting one commitment via [insert_pow] is equivalent
// to doing so via [insert_list [c]].
#push-options "--z3rlimit 50"
val lemma_insert_list_singleton :
  h:C.v_height ->
  c:C.t_C ->
  t:imt h{count_leaves t < pow2 (v63 h)} ->
  Lemma (ensures (insert_pow' c (count_leaves' h t) h t
                    = insert_list [c] (count_leaves' h t) h t))
        (decreases %[v63 h;0])
val lemma_insert_node_empty_singleton :
  h:int63{Int63.(h >=^ lit 0 /\ h <^ C.max_height)} ->
  c:C.t_C ->
  l:imt h{Empty? l} ->
  r:imt h{Empty? r} ->
  Lemma (requires (count_leaves l + count_leaves r < pow2 (v63 h + 1)))
        (ensures (insert_pow' c 0ul (suc63 h) Empty =
                      insert_node h Empty Empty 0ul [c]))
        (decreases %[v63 h;1])

val lemma_insert_node_singleton :
  h:int63{Int63.(h >=^ lit 0  /\ h <^ C.max_height)} ->
  c:C.t_C ->
  l:imt h{not (Empty? l)} ->
  r:imt h{incremental (suc63 h) (Node (hash h l r) l r)} ->
  Lemma (requires (count_leaves l + count_leaves r < pow2 (v63 h + 1)))
        (ensures (let t = Node (hash h l r) l r in
                  insert_pow' c (count_leaves' (suc63 h) t) (suc63 h) t
                    = insert_node h l r (count_leaves' (suc63 h) t) [c]))
        (decreases %[v63 h;1])
let rec lemma_insert_list_singleton h c t =
  match (t, Int63.(h =^ lit 0)) with
  | (Empty, true) -> ()
  | (Empty, _) -> lemma_insert_node_empty_singleton (pred63 h) c Empty Empty
  | (Node _ l r, _) ->
    lemma_insert_node_singleton (pred63 h) c l r
and lemma_insert_node_empty_singleton h c l r =
  let t = Node (hash h l r) l r in
  let pos = count_leaves t in
  assert(pos < pow2 (v63 h));
  lemma_insert_list_singleton h c l
and lemma_insert_node_singleton h c l r =
  let t = Node (hash h l r) l r in
  let pos = count_leaves t in
  if pos >= pow2 (v63 h) then (
    lemma_tree_leaves_bound h r;
    lemma_incremental_full_left (suc63 h) t;
    max_leaves_full h r;
    lemma_insert_list_singleton h c r)
  else (
    lemma_insert_list_singleton h c l;
    let l' = insert_list [c] (count_leaves' h l) h l in
    lemma_incremental_empty_right (suc63 h) t);
  assert(insert_pow' c (count_leaves' (suc63 h) t) (suc63 h) t
                    = insert_node h l r (count_leaves' (suc63 h) t) [c])
#pop-options

// Smart projection functions.
val left : h:C.v_height{Int63.(h >^ lit 0)} -> t:imt h -> Tot (imt (pred63 h))
let left h t =
    match t with
    | Empty -> Empty
    | Node _ l  _ -> l

val right : h:C.v_height{Int63.(h >^ lit 0)} -> t:imt h -> Tot (imt (pred63 h))
let right h t =
    match t with
    | Empty -> Empty
    | Node _ _  r -> r

///////////////////////////////////////////////////////////////////////////////////
// The following lemmas are used in the proof of [lemma_insert_list_append_distrib],
// which states that [insert_list] is distributibutive wrt append.
///////////////////////////////////////////////////////////////////////////////////

// [lem_insert_lists_subtrees_left] states that given an IMT [t] whose left-subtree
// isn't full and  a list of commitments [vs1] that fits in its left-subtree, then
// inserting this list into [t] is equivalent to inserting [vs1] into the left-subtree
// and keeping the right-subtree as it was.
val lem_insert_lists_subtrees_left :
    h:C.v_height{Int63.(h >^ lit 0)} ->
    vs1:list C.t_C{length vs1 > 0} ->
    pos:uint32{U32.v pos < pow2 (v63 h - 1)} ->
    t:imt h{U32.v pos = count_leaves t} ->
    Lemma (requires (U32.v pos + length vs1 <= pow2 (v63 h - 1) /\
		     U32.v pos = count_leaves (left h t) /\
		     incremental (pred63 h) (left h t) /\
		     Empty? (right h t)))
          (ensures ( (insert_list vs1 pos h t) = (
	               let l' = insert_list vs1 pos (pred63 h) (left h t) in
		       let r' = right h t in
		       Node (hash (pred63 h) l' r') l' r')))
#push-options "--z3rlimit 20"
let lem_insert_lists_subtrees_left h vs1 pos t =
  match (t, h) with
  | (Empty, h) ->
    assert(insert_list vs1 pos h t = insert_node (pred63 h) Empty Empty pos vs1);
    let at = U32.sub (pow2_32 (pred63 h)) pos in
    let vsl, vsr = split_at at vs1 in ()
  | (Node _ l r, h) ->
    assert(insert_list vs1 pos h t = insert_node (pred63 h) l r pos vs1);
    let at = U32.sub (pow2_32 (pred63 h)) pos in
    let vsl, vsr = split_at at vs1 in
    let l' = insert_list vsl pos (pred63 h) l in
    ()
#pop-options

// [lem_insert_lists_subtrees_right] states that given an IMT [t] whose left-subtree
// is full and a list of commitments [vs2] that fits in its right-subtree, then
// inserting this list into [t] is equivalent to inserting [vs2] into the right-subtree
// and keeping the left-subtree as it was.
val lem_insert_lists_subtrees_right :
    h:C.v_height{Int63.(h >^ lit 0)} ->
    vs2:list C.t_C ->
    pos:uint32{U32.v pos = pow2 (v63 h - 1)} ->
    t:imt h{U32.v pos = count_leaves t} ->
    Lemma (requires (U32.v pos + length vs2 <= pow2 (v63 h) /\
		     U32.v pos = count_leaves (left h t) /\
		     Empty? (right h t)))
          (ensures ( (insert_list vs2 pos h t) = (
	               let l' = left h t in
		       let r' = insert_list vs2 0ul (pred63 h) Empty in
		       Node (hash (pred63 h) l' r') l' r')))
let lem_insert_lists_subtrees_right h vs1 pos t = ()

// Add an uint32 to the length of a list, returning an uint32
val plus_l32 :
  pos:uint32 ->
  vs1:list 'a ->
  Pure uint32
       (requires (UInt32.v pos + length vs1 < pow2 32) )
       (ensures (fun s -> U32.v s < pow2 32 /\
                       U32.v s = U32.v pos + length vs1))
let plus_l32 pos vs1 =
  FStar.Math.Lemmas.small_mod (length vs1) (pow2 32);
  lem_max_size();
  U32.(pos +^ length32 vs1)

// [lem_insert_lists_subtrees] states that given an IMT [t],
// commitment lists [vs1] and [vs2]  such that [vs1] perfectly fits in [t]'s
// left-subtree [l] and [vs2] fits in [t]'s right-subtree [r], then inserting [vs1] and
// [vs2] into [t] is equivalent to inserting [vs1] into the [l] and [vs2] into [r].
#push-options "--z3rlimit 20"
val lem_insert_lists_subtrees :
    h:C.v_height{Int63.(h >^ lit 0)} ->
    vs1:list C.t_C{length vs1 > 0} ->
    vs2:list C.t_C ->
    pos:uint32{U32.v pos < pow2 (v63 h - 1)} ->
    t:imt h{U32.v pos = count_leaves t} ->
    Lemma (requires (U32.v pos + length vs1 <= pow2 (v63 h - 1) /\
    	  	     (U32.v pos + length vs1 < pow2 (v63 h - 1) ==> vs2 = []) /\
    	  	     length vs2 <= pow2 (v63 h - 1) /\
		     U32.v pos = count_leaves (left h t) /\
		     Empty? (right h t) /\
		     UInt.fits (U32.v pos + length vs1) 32))
          (ensures ((insert_list vs2 (plus_l32 pos vs1) h
      	              (insert_list vs1 pos h t)) = (
	               let l' = insert_list vs1 pos (pred63 h) (left h t) in
		       let r' = insert_list vs2 0ul (pred63 h) (right h t) in
		       Node (hash (pred63 h) l' r') l' r')))
let lem_insert_lists_subtrees h vs1 vs2 pos t =
  lem_insert_lists_subtrees_left h vs1 pos t;
  let t' = insert_list vs1 pos h t in
  if (U32.v pos + length vs1 = pow2 (v63 h - 1))
  then (lem_insert_lists_subtrees_right h vs2 (plus_l32 pos vs1) t')
  else (assert(vs2 = []); ())
#pop-options

// The result of splitting a list [vs1@vs2] at [vs1]'s length is [(vs1, vs2)].
val lemma_splitAt_clean :
  #a:eqtype ->
  vs1:list a ->
  vs2:list a ->
  Lemma (requires (length vs1 < pow2 32))
        (ensures (let (sl, sr) = split_at (length32 vs1) (vs1@vs2) in
    	  	  sl = vs1 /\ sr = vs2))
#push-options "--z3rlimit 10"
let rec lemma_splitAt_clean #a vs1 vs2 =
  match vs1 with
  | [] -> ()
  | _ ::vs1' ->
    FStar.Math.Lemmas.small_mod (length vs1) (pow2 32);
    lem_max_size();
    assert(U32.v (length32 vs1) = length vs1);
    lemma_splitAt_clean vs1' vs2
#pop-options

// [lem_insert_perfect_lists_append_distrib] states that given an IMT [t], commitment
// lists [vs1] and [vs2]  such that [vs1] perfectly fits in [t]'s left-subtree [l] and
// [vs2] fits in [t]'s right-subtree [r], then inserting [vs1] and [vs2] into [t] one
// at a time is equivalent to inserting [vs1@vs2] into [t].
#push-options "--z3rlimit 100"
val lem_insert_perfect_lists_append_distrib :
    h:C.v_height{Int63.(h >^ lit 0)} ->
    vs1:list C.t_C ->
    vs2:list C.t_C ->
    pos:uint32{U32.v pos <= pow2 (v63 h - 1)} ->
    t:imt h{U32.v pos = count_leaves t} ->
    Lemma (requires (U32.v pos + length vs1 <= pow2 (v63 h - 1) /\
                    (U32.v pos + length vs1 < pow2 (v63 h - 1) ==> vs2 = []) /\
                    length vs2 <= pow2 (v63 h - 1) /\
		    U32.v pos + length vs1 < pow2 32
		    ))
      	  (ensures (insert_list vs2 (plus_l32 pos vs1) h
      	              (insert_list vs1 pos h t) =
	            insert_list (vs1@vs2) pos h t))
          (decreases %[v63 h;0])
val lem_insert_perfect_lists_append_distrib_n :
    h:C.v_height{Int63.(h <^ C.max_height)} ->
    vs1:list C.t_C{List.length vs1 > 0} ->
    vs2:list C.t_C{List.length vs2 > 0} ->
    pos:uint32 ->
    t:imt (suc63 h){Empty? (right (suc63 h) t)
                    /\ U32.v pos = count_leaves (left (suc63 h) t)} ->
    Lemma (requires (U32.v pos + length vs1 = pow2 (v63 h) /\
                     length vs2 <= pow2 (v63 h) /\
		     U32.v pos + length vs1 < pow2 32))
      	  (ensures (insert_list vs2 (plus_l32 pos vs1) (suc63 h)
      	              (insert_list vs1 pos (suc63 h) t) =
	            insert_list (vs1@vs2) pos (suc63 h) t))
          (decreases %[v63 h;1])
let lem_insert_perfect_lists_append_distrib_n h vs1 vs2 pos t =
   assert(U32.v pos < pow2 (v63 h));
   let l = left (suc63 h) t in
   let r = right (suc63 h) t in
   let at = U32.(pow2_32 h -^ pos) in
   let vsl, vsr = split_at at (vs1@vs2) in
   lemma_splitAt_clean vs1 vs2;
   assert(vsl = vs1 /\ vsr = vs2);
   let l' = insert_list vsl pos h l in
   let r' = insert_list vsr 0ul h r in
   lem_insert_lists_subtrees (suc63 h) vsl vsr pos t
let lem_insert_perfect_lists_append_distrib h vs1 vs2 pos t =
  match (t, h, vs1, vs2) with
  | (_, _, _, []) -> ()
  | (_, _, [], _) -> ()
  | (Empty, h, _, _) ->
    lem_insert_perfect_lists_append_distrib_n (pred63 h) vs1 vs2 pos Empty
  | (Node _ l r, h, _ , _) ->
    lemma_incremental_empty_right h t;
    lem_insert_perfect_lists_append_distrib_n (pred63 h) vs1 vs2 pos t
#pop-options

// [lem_insert_list_append_distrib_lift_left] states that if the distributivity of
// [insert_list] over [@]  holds over [t]'s left-subtree, then it also holds for [t].
val lem_insert_list_append_distrib_lift_left :
    h:C.v_height{Int63.(h <^ C.max_height)} ->
    vs1:list C.t_C{length vs1 > 0 } ->
    vs2:list C.t_C{length vs2 > 0} ->
    pos:uint32 ->
    t:imt (suc63 h){U32.v pos = count_leaves t /\ Empty? (right (suc63 h) t)} ->
    Lemma (requires (U32.v pos + length vs1 + length vs2 <= pow2 (v63 h)
                     /\ U32.v pos = count_leaves (left (suc63 h) t)
		     /\ U32.v pos + length vs1 < pow2 32
                     /\ insert_list (vs1@vs2) pos h (left (suc63 h) t) =
			 insert_list vs2 (plus_l32 pos vs1) h (
			   insert_list vs1 pos h (left (suc63 h) t))))
          (ensures (insert_list (vs1@vs2) pos (suc63 h) t =
		      insert_list vs2 (plus_l32 pos vs1) (suc63 h) (
			insert_list vs1 pos (suc63 h) t)))
#push-options "--z3rlimit 20"
let lem_insert_list_append_distrib_lift_left h vs1 vs2 pos t=
  // Inserting (vs1@vs2) in t is equivalent to inserting it in its left-subtree
  lem_insert_lists_subtrees_left (suc63 h) (vs1@vs2) pos t;
  // Inserting vs1 in t is equivalent to inserting it in its left-subtree
  lem_insert_lists_subtrees_left (suc63 h) vs1 pos t;
  // Inserting vs2 in the result of inserting vs1 in t is equivalent to
  // inserting vs2 in its left-subtree
  lem_insert_lists_subtrees_left (suc63 h) vs2 (plus_l32 pos vs1)
    (insert_list vs1 pos (suc63 h) t)
#pop-options

// [lem_insert_list_append_distrib_lift_right] states that if the distributivity of
// [insert_list] over [@] holds over [t]'s right-subtree, then it also holds for [t].
#push-options "--z3rlimit 100"
val lem_insert_list_append_distrib_lift_right :
    h:C.v_height{Int63.(h <^ C.max_height)} ->
    vs1:list C.t_C ->
    vs2:list C.t_C ->
    pos:uint32{U32.v pos >= pow2 (v63 h)
               /\ length (vs1 @ vs2) <= pow2 (v63 h) - (U32.v pos - pow2 (v63 h))} ->
    l:imt h{count_leaves l = pow2 (v63 h)} ->
    r:imt h ->
    Lemma (requires (let t = (Node (hash h l r) l r) in
     	             U32.v pos = count_leaves l + count_leaves r /\
    	  	     incremental (suc63 h) t /\
                     U32.v pos + length vs1 + length vs2 <= pow2 (v63 h + 1) /\
		     U32.v pos + length vs1 < pow2 32 /\
		     U32.v pos - pow2 (v63 h) = count_leaves r) /\
                     insert_list (vs1@vs2) U32.(pos -^ pow2_32 h) h r =
                     insert_list vs2 (plus_l32 U32.(pos -^ pow2_32 h) vs1) h
                      (insert_list vs1 U32.(pos -^ pow2_32 h) h r))
          (ensures (insert_list (vs1@vs2) pos (suc63 h) (Node (hash h l r) l r) =
		      insert_list vs2 (plus_l32 pos vs1) (suc63 h) (
			insert_list vs1 pos (suc63 h) (Node (hash h l r) l r))))
let lem_insert_list_append_distrib_lift_right h vs1 vs2 pos l r =
  let t = Node (hash h l r) l r in
  assert(count_leaves l = pow2 (v63 h));
  let pos' = U32.(pos -^ pow2_32 h) in
  let r' = insert_list (vs1@vs2) pos' h r in
  assert(insert_list (vs1@vs2) pos (suc63 h) t =
           Node (hash h l r') l r');
  assert(insert_list vs2 (plus_l32 pos vs1) (suc63 h) (
           insert_list vs1 pos (suc63 h) t) =
	 Node (hash h l r') l r')
#pop-options

///////////////////////////////////////////////////////////////////////////////////
// The proof of [lemma_insert_list_append_distrib vs1 vs2 t]
// requires considering 3 cases:
// (1) The left-subtree isn't full and vs1 fits in it. We have to split vs2.
// (2) The left-subtree isn't full but vs1 doesn't fit in it. We have to split vs1.
// (3) The left-subtree is full, we don't have to split any list.
//
// To speed up the F* check of this lemma, we prove (1) and (2) separately, in
// lemma_insert_node_append_distrib_1 and lemma_insert_node_append_distrib_2
// respectively.
// Although this means that these sublemmas need huge preconditions, they
// significantly reduce the Z3 rlimit required.
///////////////////////////////////////////////////////////////////////////////////

// First case of [lemma_insert_node_append_distrib]. [vs2] is split into [vs2l]
// and [vs2r].
#push-options "--z3rlimit 30"
val lemma_insert_node_append_distrib_1 :
  h:C.v_height{Int63.(h <^ C.max_height)} ->
  t:imt (suc63 h) ->
  pos:uint32{count_leaves (left (suc63 h) t) +
              count_leaves (right (suc63 h) t) = U32.v pos
             /\ U32.v pos < pow2 (v63 h + 1)} ->
  vs1:list C.t_C ->
  vs2l:list C.t_C ->
  vs2r:list C.t_C ->
  Lemma (requires (let l = left (suc63 h) t in
                   let r = right (suc63 h) t in
                   length vs1 > 0 /\ length (vs2l@vs2r) > 0 /\
                   length vs1 + length (vs2l@vs2r) <= (pow2 (v63 h + 1)) - U32.v pos /\
                   U32.v pos < pow2 (v63 h) /\
                   length vs1 < pow2 (v63 h) - U32.v pos /\
                   U32.v pos = count_leaves l /\
                   Empty? r /\
                   (length (vs2l@vs2r) >= (pow2 (v63 h) - U32.v pos - length vs1) ==>
                     U32.v pos + length (vs1@vs2l) = pow2 (v63 h)) /\
                   (length (vs2l@vs2r) < (pow2 (v63 h) - U32.v pos - length vs1) ==>
                     vs2r = []) /\
                   (insert_node h l r pos (vs1@(vs2l@vs2r)) =
                    (let l' = insert_list (vs1@vs2l) pos h l in
                     let r' = insert_list vs2r 0ul h r in
                     Node (hash h l' r') l' r')) /\
                   (insert_list (vs1@vs2l) (count_leaves' h l) h l
                     = insert_list vs2l (plus_l32 pos vs1) h
                        (insert_list vs1 (count_leaves' h l) h l))))
        (ensures (insert_node h (left (suc63 h) t) (right (suc63 h) t)
	                      pos (vs1@(vs2l@vs2r))
                  = insert_list (vs2l@vs2r) (plus_l32 pos vs1) (suc63 h) (
                       insert_list vs1 pos (suc63 h) t)))
let lemma_insert_node_append_distrib_1 h t pos vs1 vs2l vs2r =
  List.Tot.append_assoc vs1 vs2l vs2r;
  let l = left (suc63 h) t in

  // As we are in case 1:
  // insert (vs1@vs2l@vs2r) t =
  //  Node (insert (vs1@vs2l) l) (insert vs2r r)

  // We know that vs1@vs2l perfectly fits in l and vs2r fits in r, so
  // inserting vs1@vs2l and then vs2r into t is equivalent to
  // inserting vs1@vs2l into l and vs2r into r:
  // Node (insert (vs1@vs2l) l) (insert vs2r r) =
  //  insert vs2r (insert (vs1@vs2l) t)
  lem_insert_lists_subtrees (suc63 h) (vs1@vs2l) vs2r pos t;

  let lsz = length (vs1@vs2l) in
  // By IH, we know that the distrib prop stands for inserting (vs1@vs2l) into l,
  // so we can lift it to t:
  // insert (vs1@vs2l) t =
  //  insert vs2l (insert vs1 t)
  lem_insert_list_append_distrib_lift_left h vs1 vs2l pos t;

  // Let t' be the result of inserting vs1 into t.
  // As vs2l fits perfectly in t' 's left-subtree and vs2r on the right,
  // we know that distributivity stands:
  // insert vs2r (insert vs2l (insert vs1 t)) =
  //  insert (vs2l@vs2r) (insert vs1 t)
  lem_insert_perfect_lists_append_distrib
    (suc63 h) vs2l vs2r (plus_l32 pos vs1) (insert_list vs1 pos (suc63 h) t)
#pop-options

// Second case of [lemma_insert_node_append_distrib]. [vs1] is split into [vs1l]
// and [vs1r].
val lemma_insert_node_append_distrib_2 :
  h:C.v_height{Int63.(h <^ C.max_height)} ->
  t:imt (suc63 h) ->
  pos:uint32{count_leaves (left (suc63 h) t) +
              count_leaves (right (suc63 h) t) = U32.v pos
             /\ U32.v pos < pow2 (v63 h + 1)} ->
  vs1l:list C.t_C ->
  vs1r:list C.t_C ->
  vs2:list C.t_C ->
  Lemma (requires (let l = left (suc63 h) t in
                   let r = right (suc63 h) t in
                   let vs1 = vs1l@vs1r in
                   length vs1 > 0 /\ length vs2 > 0 /\
		   U32.v pos + length vs1 < pow2 32 /\
                   length vs1 + length vs2 <= (pow2 (v63 h + 1)) - U32.v pos /\
                   U32.v pos < pow2 (v63 h) /\
                   length vs1 >= pow2 (v63 h) - U32.v pos /\
                   U32.v pos = count_leaves l /\
                   Empty? r /\
                   length vs1l = pow2 (v63 h) - U32.v pos /\
                   (insert_node h l r pos (vs1@vs2) =
                    (let l' = insert_list vs1l pos h l in
                     let r' = insert_list (vs1r@vs2) 0ul h r in
                     Node (hash h l' r') l' r')) /\
                   (insert_list (vs1r@vs2) 0ul h r
                     = insert_list vs2 (length32 vs1r) h
                        (insert_list vs1r 0ul h r))))
        (ensures (insert_node h (left (suc63 h) t) (right (suc63 h) t)
	                      pos ((vs1l@vs1r)@vs2))
                  = insert_list vs2 (plus_l32 pos (vs1l@vs1r)) (suc63 h) (
                       insert_list (vs1l@vs1r) pos (suc63 h) t))
#push-options "--z3rlimit 50"
let lemma_insert_node_append_distrib_2 h t pos vs1l vs1r vs2 =
  let r = right (suc63 h) t in
  let l = left (suc63 h) t in
  let l' = insert_list vs1l pos h l in
  List.Tot.append_assoc vs1l vs1r vs2;
  // As we are in case 2:
  // insert ((vs1l@vs1r)@vs2) t =
  //  Node (insert vs1l l) (insert (vs1r@vs2) r)

  // We know that vs1l fits in l, so:
  // insert vs1l t =
  //  Node (insert vs1l l) r
  lem_insert_lists_subtrees_left (suc63 h) vs1l pos t;

  // As vs1l fits in l and (vs1r@vs2) fits in r:
  // insert (vs1l@(vs1r@vs2)) t =
  //  insert (vs1r@vs2) (insert vs1l t)
  lem_insert_perfect_lists_append_distrib (suc63 h) vs1l (vs1r@vs2) pos t;

  // As vs1l fits in l and (vs1r@vs2) fits in r:
  // Node (insert vs1l l) (insert (vs1r@vs2) r) =
  //  insert (vs1r@vs2) (insert vs1l t)
  lem_insert_lists_subtrees (suc63 h) vs1l (vs1r@vs2) pos t;

  // We can lift the IH to (insert vs1l t):
  // insert (vs1r@vs2) (insert vs1l t) =
  //  insert vs2 (insert vs1r (insert vs1l t))
  lem_insert_list_append_distrib_lift_right h vs1r vs2 (plus_l32 pos vs1l) l' r;

  // As vs1l fits in l and vs1r fits in r:
  //  insert vs1r (insert vs1l t) =
  //   insert (vs1l@vs1r) t
  lem_insert_perfect_lists_append_distrib (suc63 h) vs1l vs1r pos t
#pop-options

// Auxiliary lemma: the number of leaves of inserting a list into a tree t is
// the number of leaves of t plus the list's length
val lem_insert_list_leaves :
  vs:list C.t_C ->
  pos:uint32 ->
  h:C.v_height ->
  t:imt h{count_leaves t + length vs <= pow2 (v63 h) /\
          count_leaves t < pow2 (v63 h) /\
          U32.v pos = count_leaves t /\
	  U32.v pos + length vs < pow2 32} ->
  Lemma (ensures (count_leaves (insert_list vs pos h t) =
                  U32.v pos + length vs))
let lem_insert_list_leaves vs pos h t =
  assert (count_leaves t = U32.v pos)

// Distributive property of [insert_list] over [@].
val lemma_insert_list_append_distrib :
  vs1:list C.t_C ->
  vs2:list C.t_C ->
  pos:uint32 ->
  h:C.v_height ->
  t:imt h{count_leaves t + length vs1 + length vs2 <= pow2 (v63 h) /\
          count_leaves t < pow2 (v63 h) /\
          U32.v pos = count_leaves t /\
	  U32.v pos + length vs1 < pow2 32} ->
  Lemma (ensures (insert_list (vs1@vs2) (count_leaves' h t) h t
        = insert_list vs2 (plus_l32 pos vs1) h
            (insert_list vs1 (count_leaves' h t) h t)))
        (decreases %[v63 h;0])
val lemma_insert_node_append_distrib :
  h:C.v_height{Int63.(h <^ C.max_height)} ->
  t:imt (suc63 h) ->
  pos:uint32{count_leaves (left (suc63 h) t) +
              count_leaves (right (suc63 h) t) = U32.v pos
             /\ U32.v pos < pow2 (v63 h + 1)} ->
  vs1:list C.t_C{length vs1 > 0} ->
  vs2:list C.t_C ->
  Lemma (requires (length vs1 > 0 /\ length vs2 > 0 /\
                   length vs1 + length vs2 <= (pow2 (v63 h + 1)) - U32.v pos) /\
		   U32.v pos + length vs1 < pow2 32)
        (ensures (insert_node h (left (suc63 h) t) (right (suc63 h) t) pos (vs1@vs2)
                  = insert_list vs2 (plus_l32 pos vs1) (suc63 h) (
                       insert_list vs1 pos (suc63 h) t)))
        (decreases %[v63 h;1])

#push-options "--z3rlimit 200"
let rec lemma_insert_list_append_distrib vs1 vs2 pos h t =
  match (t, vs1, vs2) with
  | (_, _, []) -> ()
  | (_, [], _) -> ()
  | (Empty, _, _) ->
      lemma_insert_node_append_distrib (pred63 h) Empty pos vs1 vs2
  | (Node _ l r, _ , _) ->
      lemma_insert_node_append_distrib (pred63 h) t pos vs1 vs2

and lemma_insert_node_append_distrib h t pos vs1 vs2 =
  let l = left (suc63 h) t in
  let r = right (suc63 h) t in
  if U32.v pos < pow2 (v63 h)
  then
    // The left-subtree isn't full
    let at = U32.(pow2_32 h -^ pos) in
    let vsl, vsr = split_at at (vs1@vs2) in
    // As we don't know t's shape, we need to prove that r is Empty even if
    // t is a Node
    if (Node? t)
    then  lemma_incremental_empty_right (suc63 h) t
    else ();
    assert(Empty? r);
    assert(U32.v pos = count_leaves l);
    if (length vs1 < pow2 (v63 h) - U32.v pos)
    then begin
      // Case 1: vs1 fits in l, we need to split vs2.
      assert(U32.v (pow2_32 h) = pow2 (v63 h));
      FStar.Math.Lemmas.small_mod (length vs1) (pow2 32);
      lem_max_size();
      assert(length vs1 = U32.v (length32 vs1));
      let vs2l, vs2r = split_at U32.(pow2_32 h -^ pos -^ length32 vs1) vs2 in
      assert(vs2 = vs2l@vs2r);
      List.Tot.append_assoc vs1 vs2l vs2r;
      assert((vs1@vs2l)@vs2r = vs1@vs2);
      // Either either vs2 has enough items to fill up l, or vs2r is empty
      assert(length vs2 >= (pow2 (v63 h) - U32.v pos - length vs1) ==>
               U32.v pos + length (vs1@vs2l) = pow2 (v63 h));
      assert(length vs2 < (pow2 (v63 h) - U32.v pos - length vs1) ==>
               vs2r = [] /\ vsr = []);
      if (length vs2 >= (pow2 (v63 h) - U32.v pos - length vs1))
      then
       lemma_splitAt_clean (vs1@vs2l) vs2r
      else ();
      assert(vsr = vs2r);
      assert(vs1@vs2l = vsl);
      //IH
      lemma_insert_list_append_distrib vs1 vs2l pos h l;

      lemma_insert_node_append_distrib_1 h t pos vs1 vs2l vs2r
      end
    else
      // Case 2: vs1 doesn't fit in l, we need to split vs1
      let vs1l, vs1r = split_at U32.(pow2_32 h -^ pos) vs1 in
      assert(length vs1 >= pow2 (v63 h) - U32.v pos);
      // vs1l fills up l
      assert(length vs1l = pow2 (v63 h) - U32.v pos);
      assert(vs1 = vs1l @ vs1r);
      List.Tot.append_assoc vs1l vs1r vs2;
      assert(vs1l@(vs1r@vs2) = (vs1l@vs1r)@vs2);
      lemma_splitAt_clean vs1l (vs1r@vs2);
      assert(vs1l = vsl);
      assert(vs1r@vs2 = vsr);

      //IH
      lemma_insert_list_append_distrib vs1r vs2 0ul h r;

      lemma_insert_node_append_distrib_2 h t pos vs1l vs1r vs2
  else
    // Case 3: l is full, we just insert everything into r
    let pos' = U32.(pos -^ pow2_32 h) in
    lemma_incremental_full_left (suc63 h) t;

    // IH: distributibutivity holds over r
    lemma_insert_list_append_distrib vs1 vs2 pos' h r;
    // We just lift the IH to t
    lem_insert_list_append_distrib_lift_right h vs1 vs2 pos l r
#pop-options


// Folding a list of commitments using [insert_pow'].
// [rec_inserts h [x1;x2;…;xn] t] := insert xn (… (insert x2 (insert x1 t)))
// We prove that:
// insert xn (… (insert x2 (insert x1 t))) = insert_list [x1;x2;…;xn] t
val rec_inserts :
  h:C.v_height ->
  l:list C.t_C ->
  t:imt h{count_leaves t + length l <= pow2 (v63 h)} ->
  Pure (imt h)
       (requires (count_leaves t < pow2 32))
       (ensures (fun t' -> count_leaves t' = count_leaves t + length l /\
                t' = insert_list l (count_leaves' h t) h t))
#push-options "--z3rlimit 30"
let rec rec_inserts h l t =
  match l with
  | [] -> t
  | hd::[] ->
    let t1 = insert_pow' hd (count_leaves' h t) h t in
    lemma_insert_list_singleton h hd t;
    t1
  | hd::tl ->
    let t1 = insert_pow' hd (count_leaves' h t) h t in
    FStar.Math.Lemmas.pow2_le_compat 32 (v63 h);
    let t2 = rec_inserts h tl t1 in
    lemma_insert_list_singleton h hd t;
    lemma_insert_list_append_distrib [hd] tl (count_leaves' h t) h t;
    t2
#pop-options
