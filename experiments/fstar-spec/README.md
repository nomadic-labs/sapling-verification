# Functional Verification of the Sapling Storage Library in F*
## Verification of ADT implementation
The files for the ADT specification are:
 * `Tree.fst`: Partial implementation and verification of Incremental Merkle Trees, i.e. the `Tree` module from `lib_sapling/storage.ml`.
 * `Commitments.fst`: Axiomatized model of commitments and hashes.
 * `FullMTree.fst`: Implementation and verification of full Merkle trees, inspired by this [example](https://github.com/FStarLang/FStar/blob/master/examples/data_structures/MerkleTree.fst). The goal of this module is to show that every compressed incremental Merkle tree as defined in the Tree module can be translated into a full Merkle tree.

## Building

The Makefile requires either to have `fstar.exe` in your PATH or to define `FSTAR_HOME` to be the path to the `FStar` directory in your environment.

F* version used: built from source (SHA: eea0880d54aaa2e140c18975e405fa94f4024c0f).