open Tezos_sapling
module C = Core.Validator
module H = C.Hash

type t_C = C.Commitment.t
type t_H = H.t
type v_height = int

let max_height = 32
let uncommitted h = H.uncommitted h
let hash_of_commitment = H.of_commitment
let hash_to_commitment = H.to_commitment
let merkle_hash h a b = 
  H.merkle_hash h a b
let perfect_merkle _ = ()
let hash_to_of_iso0 _ = ()
let hash_to_of_iso1 _ = ()
let uncommitted_spec _ = ()