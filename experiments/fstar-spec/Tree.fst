(* The MIT License (MIT)

 Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. *)

// Incremental Merkle Trees

// _Incremental Merkle Trees_ are an optimization of full Merkle trees
// of fixed height.

// A full Merkle tree of height h contains 2 ^ h leaves, and h+1
// levels of nodes, with leaves at level 0 and its root at level h. In
// the Tezos codebase, we use IMTs of fixed height (32 in the sapling
// storage library for Tezos since the Edo(008) protocol). Trees of
// fixed size enable in turn proofs of membership---witnesses---, of
// fixed size.

// Unlike regular Merkle trees, Incremental Merkle Trees can be indeed
// Empty. Intuitively, an empty IMT models a complete Merkle tree
// where all leaves are pre-filled with a default value, given by
// uncommitted 0.

// IMTs are _incremental_ in the sense that leaves cannot be modified
// but only added, and they are moreover inserted exclusively in
// successive positions from left to right. IMTs then optimize both
// the size of the underlying Merkle tree and the computation of known
// hashes for default values:

// - Internally, a sub-tree filled with default values is represented
// by an Empty tree, avoiding to construct the sub-tree.

// - Since all the nodes at the same level of an underlying Empty
// sub-tree have the same hash, it can be computed from the default
// value of the leaves. This is stored in the uncommitted list, with
// index n > 0.

module Tree

module C = Commitments

module UInt32 = FStar.UInt32
type int63 = Int63.t
type uint32 = UInt32.t
module Int64 = FStar.Int64
type int64 = Int64.t

val lit : i:(FStar.Int.int_t 63){i >= 0 /\ i <= Int63.v C.max_height} ->
          b:C.v_height{Int63.v b = i}
let lit i = Int63.int_to_t i

val pred63 : x:C.v_height{Int63.(x >^ lit 0)} -> C.v_height
let pred63 x = Int63.(x -^ lit 1)

val suc63 : x:int63{Int63.(x >=^ lit 0 /\ x <^ C.max_height)} -> C.v_height
let suc63 x = Int63.(x +^ lit 1)

val v63 : h:C.v_height -> Tot (n:nat{n = Int63.v h})
let v63 h = assert(Int63.(h >=^ lit 0)); Int63.v h

// Storing the height in Empty would simplify everything, but we don't
// want to do it in the key value store implementtion in the
// protocol. So, instead, we always pass the height as argument.

// 'a must be an eqtype only if we want to define a `mem` function.
type tree 'a 'b : Type =
  | Empty : tree 'a 'b
  | Leaf : 'a -> tree 'a 'b
  | Node : 'b -> l:tree 'a 'b -> r:tree 'a 'b -> tree 'a 'b

// All trees of height 3 with nodes indexed from 1 to 7:
//    x          x
//  x   x   or
// x x x x
let t0 = Empty #nat #nat
//    v
//  v   x
// o x
let t1 : tree nat nat = Node 0 (Node 0 (Leaf 0) Empty) Empty
//    v
//  v   x
// o o
let t2 : tree nat nat = Node 2 (Node 1 (Leaf 0) (Leaf 1)) Empty
//    v
//  v   v
// o o o x
let t3 : tree nat nat = Node 2 (Node 1 (Leaf 0) (Leaf 1)) (Node 1 (Leaf 2) Empty)
//    v
//  v   v
// o o o o
let t4 : tree nat nat = Node 2 (Node 1 (Leaf 0) (Leaf 1)) (Node 1 (Leaf 2) (Leaf 3))

let _ = assert (Node?.r t3 = (Node 1 (Leaf 2) Empty))

// valid checks wether we put Empty constructors in the right place:
// either if the tree is really empty, or to represent a _cut_ of a
// sub-tree full of default values. Thus, we shoud never have a `Node
// Empty Empty` tree, as this would allow to construct sub-trees with
// gaps.
val valid : tree 'a 'b -> bool
let rec valid = function
  | Empty -> true
  | Leaf _ -> true
  | Node _ Empty Empty -> false
  | Node _ l r -> valid l && valid r

// A node is full if all paths from the root finish in a Leaf. Thus,
// there is no Empty subnode of a full tree.

val full : tree 'a 'b -> Tot bool
let rec full  = function
  | Empty -> false
  | Leaf _ -> true
  | Node _ l r -> (full l) && (full r)

val full_is_valid : t:tree 'a 'b ->
    Lemma (requires (full t))
          (ensures (valid t))
          [SMTPat (full t)]
let rec full_is_valid  = function
  | Leaf _ -> ()
  | Node _ l r -> full_is_valid l; full_is_valid r

// Incremental Merkle Trees are filled _incrementally_ from left to
// right, without gaps. That is, without any [Empty] subtree in
// between leaves at the same height:

val left_leaning : t:tree 'a 'b -> Tot bool
let rec left_leaning  = function
  | Empty -> true
  | Leaf _ -> true
  | Node _ l Empty -> left_leaning l
  | Node _ l r -> (full l) && (left_leaning r)

let t1_wrong = Node 2 (Node 1 Empty (Leaf 4)) Empty
let t2_wrong = Node 2 Empty (Node 1 (Leaf 4) (Leaf 5))
let t3_wrong = Node 2 (Node 1 (Leaf 4) (Leaf 5)) (Node 1 Empty (Leaf 6))
let t3'_wrong = Node 2 (Node 1 (Leaf 6) Empty) (Node 2 (Leaf 4) (Leaf 5))

let _ = assert (left_leaning t0)
let _ = assert (left_leaning t1)
let _ = assert (left_leaning t2)
let _ = assert (left_leaning t3)
let _ = assert (left_leaning t4)
let _ = assert (not (left_leaning t1_wrong))
let _ = assert (not (left_leaning t2_wrong))
let _ = assert (not (left_leaning t3_wrong))
let _ = assert (not (left_leaning t3'_wrong))

val full_leans_left : t:tree 'a 'b ->
    Lemma (requires True) (ensures (full t ==> left_leaning t))
    [SMTPat (full t)]
let rec full_leans_left = function
  | Empty | Leaf _ -> ()
  | Node _ l r -> full_leans_left l ; full_leans_left r

// TODO this is only used for the spec of insert and forces the type
// to be eqtype, can we remove it?
val mem : #a:eqtype -> #b:Type -> a -> tree a b -> Tot bool
let rec mem #a #_ cm = function
  | Empty -> false
  | Leaf v -> cm = v
  | Node _ l r -> mem cm l || mem cm r

// t has at least one leaf at depth `height t`
// works also on unbalanced
val height : (t:tree 'a 'b{valid t /\ not (Empty? t)}) -> Tot nat
let rec height = function
  | Leaf _ -> 0
  | Node _ l r ->
      match l,r with
      | Empty, _ -> height r + 1
      | _, Empty -> height l + 1
      | _ ->
        let hl = height l in
        let hr = height r in
        if hl > hr then (hl+1) else (hr+1) // couldn't find max;


let unbal1 = Node 2 (Leaf 2) (Node 1 (Leaf 3) Empty)

let _ = assert (height t1 = 2)
let _ = assert (height unbal1 = 2)


// The has_height predicate binds the fact that the h argument in the
// specification should prescribe the height of the IMT. Since we
// overload the Empty constructor to also denote the optimization of a
// subtree full of pre-defined values, we will allow Empty trees to
// "declare" any height. Thus, has_height is indeed an upper bound on
// the height of the tree.

val has_height : h:C.v_height -> t:tree 'a 'b{valid t} -> bool
let has_height h  = function
    | Empty -> true
    | t -> height t = Int63.v h

// t0 can be of any height
let _ = assert (has_height (lit 2) t0)
let _ = assert (has_height (lit 3) t0)
let _ = assert (has_height (lit 2) t1)
let _ = assert (not (has_height (lit 3) t1))
let _ = assert (has_height (lit 2) t2)
let _ = assert (has_height (lit 2) t3)
let _ = assert (has_height (lit 2) t4)
let _ = assert (has_height (lit 2) t1_wrong)
let _ = assert (has_height (lit 2) t2_wrong)
let _ = assert (has_height (lit 2) t3_wrong)
let _ = assert (has_height (lit 2) t3'_wrong)
let _ = assert (not (has_height (lit 1) unbal1))
let _ = assert (has_height (lit 2) unbal1)
let _ = assert (not (has_height (lit 3) unbal1))

// IMTs model complete, balanced Merkle trees. However, since they do
// so by overloading Empty to denote _cuts_, we need a tailored
// version of the traditional definition of a _balanced binary tree_
// which accounts for the left-leaning skew.

val balanced : t:tree 'a 'b{valid t} -> bool
let rec balanced = function
  | Empty -> true
  | Leaf _ -> true
  | Node _ l r ->
    match l,r with
    | Empty, _ -> balanced r
    | _, Empty -> balanced l
    | _ ->
        let hl = height l in
        let hr = height r in
        hl = hr && balanced l && balanced r

let _ = assert (balanced t0)
let _ = assert (balanced t1)
let _ = assert (balanced t2)
let _ = assert (balanced t3)
let _ = assert (balanced t4)
let _ = assert (balanced t1_wrong)
let _ = assert (balanced t2_wrong)
let _ = assert (balanced t3_wrong)
let _ = assert (balanced t3'_wrong)
let _ = assert (not (balanced unbal1))


// We pack the four properties that make a tree a proper Incremental
// Merkle tree of height h. This will serve as the precondition of all
// functions in internal/lower-level API below.

val incremental : h:C.v_height -> t:tree 'a 'b -> bool
let incremental h t =
    valid t && left_leaning t && balanced t && has_height h t

// number of leaves of a tree
val count_leaves : tree 'a 'b -> Tot nat
let rec count_leaves = function
  | Empty -> 0
  | Leaf _ -> 1
  | Node _ l r -> (count_leaves l) + (count_leaves r)


val max_leaves : h:C.v_height -> t:tree 'a 'b ->
  Lemma (requires (valid t /\ has_height h t /\ balanced t))
        (ensures (count_leaves t <= pow2 (v63 h)))
	(decreases %[v63 h])
let rec max_leaves h t =
    match t with
    | Empty | Leaf _ -> ()
    | Node _ l r ->
        max_leaves (pred63 h) l;
        max_leaves (pred63 h) r

val max_leaves_full : h:C.v_height -> t:tree 'a 'b ->
  Lemma (requires (valid t /\ has_height h t /\ balanced t))
        (ensures (count_leaves t = pow2 (v63 h) <==> full t))
	(decreases %[v63 h])
let rec max_leaves_full  h t =
    match t with
    | Empty | Leaf _ -> ()
    | Node _ l r ->
      let hs = pred63 h in
      max_leaves hs l;
      max_leaves hs r;
      max_leaves_full hs l;
      max_leaves_full hs r

val lemma_tree_leaves_bound :
  h:C.v_height ->
  t:tree 'a 'b{incremental h t} ->
  Lemma (requires True)
  	(ensures (count_leaves t <= pow2 (v63 h)))
  	(decreases %[v63 h])
let rec lemma_tree_leaves_bound h t =
  match t with
  | Empty -> ()
  | Leaf _ -> ()
  | Node _ l r ->
    lemma_tree_leaves_bound (pred63 h) l;
    lemma_tree_leaves_bound (pred63 h) r

val lemma_incremental_full_left :
  h:C.v_height ->
  t:tree 'a 'b{incremental h t} ->
  Lemma (requires (Node? t /\ count_leaves t >= pow2 ((v63 h) - 1)))
        (ensures (count_leaves (Node?.l t) = pow2 ((v63 h) - 1)))
let lemma_incremental_full_left h t =
  let (Node _ l r) = t in
  if full l then
    max_leaves_full (pred63 h) l
  else
    lemma_tree_leaves_bound (pred63 h) l

val lemma_incremental_empty_right :
  h:C.v_height ->
  t:tree 'a 'b{incremental h t} ->
  Lemma (requires (Node? t /\ count_leaves t < (pow2 ((v63 h) - 1))))
        (ensures (count_leaves (Node?.l t) = count_leaves t /\
                  Empty? (Node?.r t)))
let lemma_incremental_empty_right h t =
  let (Node _ l r) = t in
  if full l then (
    max_leaves_full (pred63 h) l;
    assert(count_leaves l = pow2 (v63 (pred63 h)));
    assert(count_leaves t > count_leaves l)
    )
  else
    lemma_tree_leaves_bound (pred63 h) l

val to_list : tree 'a 'b -> list 'a
let rec to_list = function
  | Empty -> []
  | Leaf v -> [v]
  | Node _ l r -> (to_list l) @ (to_list r)


val lemma_to_list_nil : #a:eqtype -> #b:Type -> t:tree a b ->
    Lemma (requires (valid t))
          (ensures (not (Empty? t) <==> to_list t <> []))
let rec lemma_to_list_nil #a #b = function
  | Empty -> ()
  | Leaf v -> ()
  | Node _ l r -> lemma_to_list_nil l ; lemma_to_list_nil r

val lemma_length : #a:eqtype -> #b:Type -> t:tree a b ->
    Lemma (ensures (count_leaves t = List.Tot.length (to_list t)))
          [SMTPat (count_leaves t)]
let rec lemma_length #a #b = function
  | Empty -> ()
  | Leaf v -> ()
  | Node _ l r ->
    lemma_length l;
    lemma_length r;
    List.Tot.append_length (to_list l) (to_list r)

val lemma_nth_index : #a:eqtype -> l:list a ->
    Lemma (ensures forall (n:nat). (n < List.length l ==>
                      (List.Tot.nth l n = Some (List.Tot.index l n)))
                /\ (n >= List.length l ==> List.Tot.nth l n = None))
let rec lemma_nth_index #a l = match l with
  | [] -> ()
  | v::vs -> lemma_nth_index vs

open FStar.List.Tot

val lemma_index_prefix : #a:eqtype -> l1:list a -> l2:list a -> i:nat ->
    Lemma (requires (i < List.length l1))
          (ensures (List.Tot.index l1 i = List.Tot.index (l1@l2) i))
let rec lemma_index_prefix #a l1 l2 i =
match l1 with
| [] -> ()
| e::es ->
    if i = 0 then () else
    lemma_index_prefix es l2 (i-1)

val lemma_index_postfix : #a:eqtype -> l1:list a -> l2:list a -> i:nat ->
    Lemma (ensures (i >= List.length l1 /\ i < List.length (l1@l2)
                   ==> (List.Tot.index l2 (i- List.length l1) =
                      List.Tot.index (l1@l2) i)))
let rec lemma_index_postfix #a l1 l2 i =
match l1 with
| [] -> ()
| e::es ->
    if i = 0 then () else
    lemma_index_postfix es l2 (i-1)

///////////////////
// Internal API
//////////////////

// Gets the root hash at a certain [height]. If the tree is [Empty],
// we use the [uncomitted] list to get the corresponding hash.
val get_root_height : t:tree C.t_C C.t_H -> h:C.v_height ->
  Pure C.t_H (requires (valid t /\ has_height h t))
             (fun _ -> True)
let get_root_height tree height =
  //assert (Compare.Int.(height >= 0 && height <= 32)) ;
  match tree with
  | Empty ->
      C.uncommitted height
  | Node h _ _ ->
      h
  | Leaf h ->
      C.hash_of_commitment h

val pow2_32 : e:int63{Int63.(e >=^ lit 0 /\ e <^ C.max_height)} -> uint32
let pow2_32 e =
  let e32 = Int63.to_uint32 e in
  UInt32.shift_left 1ul e32

val pow2_32_correct : e:int63{Int63.(e >=^ lit 0 /\ e <^ C.max_height)} ->
  Lemma (ensures ((UInt32.v (pow2_32 e)) == pow2 (v63 e)))
        [SMTPat (pow2_32 e)]
let pow2_32_correct e =
    let e32 = UInt32.uint_to_t (v63 e) in
    FStar.UInt.shift_left_value_lemma #32 1 (v63 e);
    FStar.Math.Lemmas.pow2_lt_compat 32 (UInt32.v e32)

val get : #a:eqtype -> #b:Type -> h:C.v_height ->
    t:tree a b {incremental h t} ->
    pos:uint32 {UInt32.v pos < count_leaves t} ->
    Tot (o:a {List.Tot.index (to_list t) (UInt32.v pos) = o})
        (decreases %[v63 h])
let rec get #a #b h t pos = match t with
  | Leaf v -> v
  | Node _ l r ->
    let hs = pred63 h in
    max_leaves hs l;
    max_leaves_full hs l;
    if UInt32.(pos <^ pow2_32 hs)
    then (
      lemma_index_prefix (to_list l) (to_list r) (UInt32.v pos);
      get hs l pos)
    else (
      lemma_index_postfix (to_list l) (to_list r) (UInt32.v pos);
      get hs r UInt32.(pos -^ pow2_32 hs))

let _ = assert (get (lit 2) t1 0ul = 0)
let _ = assert (get (lit 2) t2 1ul = 1)
let _ = assert (get (lit 2) t3 2ul = 2)
let _ = assert (get (lit 2) t4 3ul = 3)

// Apply the [merkle_hash] function over the root hashes of
// the subtrees [t1] and [t2].
val hash : h:C.v_height ->
  t1:tree C.t_C C.t_H{(valid t1 /\ has_height h t1)} ->
  t2:tree C.t_C C.t_H{(valid t2 /\ has_height h t2)} -> C.t_H
let hash height t1 t2 =
  C.merkle_hash
    height
    (get_root_height t1 height)
    (get_root_height t2 height)

// Merkle tree property.
val merkle : h:C.v_height ->
  t:tree C.t_C C.t_H{valid t /\ has_height h t /\ balanced t} ->
  Tot bool
  (decreases %[v63 h])
let rec merkle h t =
  match t with
  | Empty -> true
  | Leaf _ -> true
  | Node ha l r ->
    let hs = pred63 h in
    ha = hash hs l r && merkle hs l && merkle hs r

// Type synonym for "compressed" IMTs of height h.
type imt (h:C.v_height) : Type  =
   t:tree C.t_C C.t_H {incremental h t /\ merkle h t}

// Naive insert implementation: we check on each node
// wether the left-subtree is full to determine whether we have to
// descend on the left or right sub-tree.

val insert_model :
    v:C.t_C ->
    h:C.v_height ->
    t:imt h{not (full t)} ->
    Pure (imt h)
    	 (requires (count_leaves t < pow2 32 ))
         (ensures (fun t' -> count_leaves t' = count_leaves t + 1
	                  /\ get h t' (UInt32.uint_to_t (count_leaves t)) = v))
	 (decreases %[v63 h])
#push-options "--z3rlimit 40"
let rec insert_model v h t = match t with
  | Empty ->
      if Int63.(h =^ lit 0) then Leaf v
      else
        let l = insert_model v (pred63 h) Empty in
        let ha = hash (pred63 h) l Empty in
        Node ha l Empty
  | Node _ l r ->
      max_leaves (pred63 h) l;
      max_leaves_full (pred63 h) l;
      max_leaves (pred63 h) r;
      if full l
      then
        let r = insert_model v (pred63 h) r in
        let ha = hash (pred63 h) l r in
        Node ha l r
      else
        let l = insert_model v (pred63 h) l in
        let ha = hash (pred63 h) l r in
        Node ha l r
#pop-options


// The first optimization we implement takes into account that (in the
// high-level API) we only insert at the next available position
// (i.e. count_leaves t). Then, the test for fullness above can be
// replaced by a check wether the position argument is greater than
// the maximum capacity of the left subtree.

val insert_pow :
  v:C.t_C ->
  pos:uint32 ->
  h:C.v_height ->
  t:imt h{not (full t)} ->
  Pure (imt h)
       (requires (UInt32.v pos = count_leaves t))
       (ensures (fun t' -> to_list t' = to_list t @ [v]
                        /\ count_leaves t' = count_leaves t + 1))
       (decreases %[v63 h])
#push-options "--z3rlimit 15"
let rec insert_pow v pos h = function
  | Empty ->
      if Int63.(h =^ lit 0) then Leaf v
      else
        let l = insert_pow v pos (pred63 h) Empty in
        let ha = hash (pred63 h) l Empty in
        Node ha l Empty
  | Node _ l r ->
      max_leaves_full (pred63 h) l;
      if UInt32.(pos >=^ pow2_32 (pred63 h))
      then
        let _ = max_leaves (pred63 h) l in
        let r' = insert_pow v UInt32.(pos -^ (pow2_32 (pred63 h)))
                   (pred63 h) r in
        let ha = hash (pred63 h) l r' in
        List.Tot.append_assoc (to_list l) (to_list r) [v];
        Node ha l r'
      else
        let l' = insert_pow v pos(pred63 h) l in
        let ha = hash (pred63 h) l' r in
        List.Tot.append_l_nil (to_list l);
        List.Tot.append_l_nil (to_list l');
        Node ha l' r
#pop-options

let insert = insert_pow

(* These examples won't work now
let _ = assert (insert #nat 0 0 2 t0 = t1)
let _ = assert (insert #nat 1 1 2 t1 = t2)
let _ = assert (insert #nat 2 2 2 t2 = t3)
let _ = assert (insert #nat 3 3 2 t3 = t4)
*)


// The next optimization allows for eficient batching insertion of
// consecutive commitments, i.e., it will insert a list of
// commitments, one next to the other, if there is enough space
// available in the tree.

val split_at :
  #a:eqtype ->
  p:uint32 ->
  l1:list a ->
  Pure (list a * list a)
       (requires True)
       (ensures (fun(l2, l3) ->
         (l2 @ l3 = l1) /\
         (UInt32.v p > length l1 ==> l2 = l1 /\ l3 = []) /\
         (UInt32.v p <= length l1 ==>
           length l2 = UInt32.v p /\ length l3 = length l1 - UInt32.v p)))
       (decreases l1)
let rec split_at #_ p l =
  if UInt32.(p =^ 0ul) then ([], l)
  else
    match l with
    | [] -> ([], l)
    | x :: xs ->
       let (l1, l2) = split_at UInt32.(p -^ 1ul) xs in
       (x :: l1, l2)


val insert_list :
  vs:list C.t_C ->
  pos:uint32 ->
  h:C.v_height ->
  t:(imt h) ->
  Pure (imt h)
       (requires (length vs <= pow2 (v63 h) - UInt32.v pos
                  /\ UInt32.v pos = count_leaves t))
       (ensures (fun t' ->  to_list t' = to_list t @ vs))
       (decreases %[v63 h;0])
val insert_node :
  h:int63{Int63.(h >=^ lit 0 /\ h <^ C.max_height)} ->
  l:imt h ->
  r:imt h ->
  pos:uint32 ->
  vs:list C.t_C {length vs > 0} ->
  Pure (imt (suc63 h))
       (requires (left_leaning (Node (hash h l r) l r)
                  /\ count_leaves l + count_leaves r = UInt32.v pos
                  /\ UInt32.v pos < pow2 (v63 (suc63 h))
                  /\ length vs <= (pow2 (v63 (suc63 h))) - UInt32.v pos))
       (ensures (fun t' -> to_list t' = to_list l @ to_list r @ vs))
       (decreases %[v63 h;1])

#push-options "--z3rlimit 50"
let rec insert_list vs pos h t =
  match (t, Int63.(h =^ lit 0), vs) with
  | (Empty, true, v::[]) ->
        Leaf v
  | (_, _, []) -> t
  | (Empty, _, _) ->
        insert_node (pred63 h) Empty Empty pos vs
  | (Node _ l r, _, _) ->
        max_leaves_full (pred63 h) l;
        List.Tot.append_assoc (to_list l) (to_list r) vs;
        insert_node (pred63 h) l r pos vs
and insert_node h l r pos vs =
  max_leaves h l;
  max_leaves_full h l;
  let (l', r') =
    if UInt32.(pos <^ pow2_32 h)
    then
      let at = UInt32.(pow2_32 h -^ pos) in
      let vsl, vsr = split_at at vs in
      let l' = insert_list vsl pos h l in
      let pos' = 0ul in
      let r' = insert_list vsr pos' h r in
      lemma_to_list_nil r';
      List.Tot.append_l_nil (to_list l);
      List.Tot.append_assoc (to_list l) vsl vsr;
      (l', r')
    else
      let pos' = UInt32.(pos -^ pow2_32 h) in
      let r' = insert_list vs pos' h r in
      (l, r')
  in
  max_leaves_full h l';
  let ha = hash h l' r' in
  Node ha l' r'
#pop-options

(* These examples won't work now
let _ = assert (insert_list [0]       0 2 t0 = t1)
let _ = assert (insert_list [0;1]     0 2 t0 = t2)
// spacing is important apparently?
let _ = assert (insert_list [0;1;2]   0 2 t0 = t3)
// spacing is important apparently?
let _ = assert (insert_list [0;1;2;3] 0 2 t0 = t4)
*)

// Batch insertion post-condition as stated in Coq (imt_model.v).
// It trivially holds from [insert_list] spec.
val iter_insert_post :
  vs:list C.t_C ->
  pos:uint32 ->
  h:C.v_height ->
  t:imt h ->
  Lemma (requires (length vs <= pow2 (v63 h) - UInt32.v pos
                   /\ UInt32.v pos = count_leaves t) )
        (ensures
          (let t' = insert_list vs pos h t in
          (forall j. get h t j =
          (if (count_leaves t <= UInt32.v j && UInt32.v j < count_leaves t + length vs)
           then List.Tot.index vs (UInt32.v j - count_leaves t)
           else get h t j))))
let iter_insert_post vs pos h t = ()

///////////////////////////
// High level interface
///////////////////////////



// n is the size of the tree and also the next position to fill
type htree a b = option uint32 * tree a b

val default_height :( h:C.v_height{Int63.(h =^ lit 32)})
let default_height = lit 32

let max_size = Int64.shift_left 1L 32ul

val size : ht:htree 'a 'b -> int64
let size (on, t) =
  match on with
  | None -> max_size
  | Some n -> Int.Cast.uint32_to_int64 n

val hvalid : htree 'a 'b -> bool
let hvalid (on, t) =
  count_leaves t = Int64.v (size (on, t)) &&
  incremental default_height t

val hmerkle : htree C.t_C C.t_H -> bool
let hmerkle (n, t) =
  valid t &&
  balanced t &&
  has_height default_height t &&
  merkle  default_height t

val hfull : htree 'a 'b -> bool
let hfull (n, t) =
    full t
    // n = pow2 default_height

val empty : ht:htree C.t_C C.t_H {hvalid ht /\ hmerkle ht}
let empty = (Some 0ul , Empty)

let hto_list (n, t) = to_list t

val v63_lit_inv : h:(FStar.Int.int_t 63){h >= 0 /\ h <= Int63.v C.max_height} ->
  Lemma (v63 (lit h) = h)
let v63_lit_inv _ = ()

// AL: Don't know why this is needed. Without it the assertion fails...
val lem_default : unit -> Lemma (v63 default_height = 32)
let lem_default _ = ()

// [hget] takes an int64 for the pos to get, and casts it into an uint32 when
// needed. We also could directly take an uint32, requiring the client to do the
// casting.
val hget :
  #a:eqtype -> #b:Type ->
  pos:int64 ->
  ht:htree a b{hvalid ht} ->
  Pure (option a)
       (requires True)
       (ensures (fun o ->
          (Int64.(pos >=^ 0L) ==> List.Tot.nth (hto_list ht) (Int64.v pos) = o) /\
	  (Int64.(pos <^ 0L) ==> None? o)))
#push-options "--z3rlimit 50"
let hget #a #b pos (os,t) =
  lemma_nth_index (to_list t);
  match (os, Int64.(pos <^ 0L || pos >=^ max_size)) with
  | _, true -> if Int64.(pos >=^ max_size)
       	      then assert(Int64.v pos >= (length (to_list t)))
	      else ();
	      None
  | None, _ -> Some (get default_height t (FStar.Int.Cast.int64_to_uint32 pos))
  | Some s, _ ->
    let pos' = (FStar.Int.Cast.int64_to_uint32 pos) in
    assert(Int64.(pos >=^ 0L));
    assert(Int64.(pos <^ max_size));
    if UInt32.(pos' <^ s)  then
    (
      max_leaves default_height t;
      lem_default ();
      assert(List.Tot.nth (to_list t) (Int64.v pos) =
             Some (get default_height t pos'));
      Some (get default_height t pos')
    )
    else
      None
#pop-options


type himt =  ht:htree C.t_C C.t_H{hvalid ht /\ hmerkle ht}


val lem_hfull_none : ht:himt ->
  Lemma (hfull ht <==> None? (fst ht))
  [SMTPat (hfull ht)]
#push-options "--z3rlimit 20"
let lem_hfull_none (on, t) =
  max_leaves_full default_height t;
  assert(None? on ==> full t);
  assert(full t ==> count_leaves t = pow2 (v63 default_height));
  assert(Some? on ==> count_leaves t = UInt32.v (Some?.v on));
  assert(not (FStar.UInt.fits (pow2 32) 32));
  lem_default ();
  assert(v63 default_height = 32);
  assert(full t ==> pow2 32 = count_leaves t);
  assert(full t ==> not (FStar.UInt.fits (count_leaves t) 32));
  assert(full t ==> None? on)
#pop-options

val add : v:C.t_C -> ht:himt{not (hfull ht)} ->
    ht':himt{hto_list ht' = hto_list ht @ [v] }
#push-options "--z3rlimit 30"
let add v (os, t) =
  let (Some s) = os in
  max_leaves_full default_height t;
  max_leaves default_height t;
  lem_default ();
  let t' = insert v s default_height t  in
  if UInt32.(s =^ lognot 0ul)
  then (
    max_leaves_full default_height t';
    (None, t'))
  else (Some (UInt32.add s 1ul), t')
#pop-options


val length' :
  l:list 'a ->
  ac:uint32 ->
  uint32
let rec length' l ac=
  match l with
  | [] -> ac
  | _::xs -> length' xs (UInt32.add_mod 1ul ac)

val lem_mod_32 :
  x:nat -> y:uint32 -> z:nat{z=Int64.v max_size} ->
  Lemma ((x + UInt32.(v (add_mod 1ul y))) % z =
  	 ((x + 1) + UInt32.v y) % z)
let lem_mod_32 x y z =
  assert(z = pow2 32);
  assert(UInt32.(v (add_mod 1ul y)) = (1 + UInt32.v y) % z);
  FStar.Math.Lemmas.lemma_mod_plus_distr_r x (1 + UInt32.v y) z

val lem_length' :
  l:list 'a ->
  ac:uint32 ->
  Lemma (UInt32.v (length' l ac) = (length l + UInt32.v ac)% Int64.v max_size)
        [SMTPat (length' l ac)]
#push-options "--z3rlimit 10"
let rec lem_length' l ac =
  match l with
  | [] -> ()
  | _::xs ->
    lem_length' xs (UInt32.add_mod 1ul ac);
    lem_mod_32 (length xs) ac (Int64.v max_size);
    assert((length xs + UInt32.(v (add_mod 1ul ac)))% Int64.v max_size =
         (length xs + 1 + UInt32.v ac) %  Int64.v max_size)
#pop-options

val length32 :
  l:list 'a ->
  Tot (y:uint32{UInt32.v y = (length l) % Int64.v max_size})
let length32 l = length' l 0ul

let lem_max_size _ : Lemma (Int64.v max_size = pow2 32) = ()

val lem_length32_correct :
  l:list 'a ->
  Lemma (requires (length l < pow2 32))
        (ensures (UInt32.v (length32 l) = length l))
	[SMTPat (length32 l)]
let lem_length32_correct l =
  lem_max_size ();
  FStar.Math.Lemmas.small_mod (length l) (pow2 32)

val lem_add_mod : x:uint32 -> y:uint32 ->
  Lemma (UInt32.(v (add_mod x y)) =
	  (UInt32.v x + UInt32.v y) % Int64.v max_size)
let lem_add_mod x y = ()

val lem_add_length : x:uint32 -> l:list 'a ->
  Lemma (UInt32.(v (add_mod x (length32 l))) =
	  (UInt32.v x + length l) % Int64.v max_size)
#push-options "--z3rlimit 10"
let lem_add_length x l =
  lem_add_mod x (length32 l);
  assert(UInt32.(v (add_mod x (length32 l))) =
	  (UInt32.v x + UInt32.v (length32 l)) % Int64.v max_size);
  assert(UInt32.v (length32 l) = length l % Int64.v max_size);
  assert(UInt32.(v (add_mod x (length32 l))) =
	  (UInt32.v x + (length l % Int64.v max_size)) % Int64.v max_size);
  FStar.Math.Lemmas.lemma_mod_plus_distr_r
   (UInt32.v x) (length l) (Int64.v max_size);
  assert(((UInt32.v x + (length l % Int64.v max_size)) % Int64.v max_size) =
         (UInt32.v x + length l) % Int64.v max_size)
#pop-options

val lem_mod_0 : a:nat -> b:nat ->
  Lemma (requires (a > 0 /\ a <= b /\ a % b = 0))
        (ensures (a = b))
let lem_mod_0 a b = ()

val lem_mod_pos : a:nat -> b:nat ->
  Lemma (requires (a > 0 /\ a <= b /\ a % b > 0))
        (ensures (a < b))
let lem_mod_pos a b = ()

val add_list :
  vs:list C.t_C ->
  ht:himt{Int64.v (size ht) <= Int64.v max_size - length vs} ->
  ht':himt{hto_list ht' = hto_list ht @ vs}
#push-options "--z3rlimit 30"
let add_list vs (os, t) =
  match vs with
  | [] -> (os, t)
  | _ ->
    let (Some s) = os in
    lem_default();
    let lvs = length32 vs in
    let t' = insert_list vs s default_height t in
    if UInt32.(add_mod s lvs =^ 0ul)
    then (
      lem_add_length s vs;
      lem_mod_0 (UInt32.v s + length vs) (Int64.v max_size);
      max_leaves_full default_height t';
      lem_max_size ();
      (None, t')
    )
    else (
      lem_add_length s vs;
      lem_mod_pos (UInt32.v s + length vs) (Int64.v max_size);
      lem_max_size();
      FStar.Math.Lemmas.small_modulo_lemma_1 (length vs) (Int64.v max_size);
      (Some (UInt32.add s lvs), t'))
#pop-options

// val membership : #a:eqtype -> ht:htree a -> ht':htree a ->
//   Lemma (requires (hvalid ht /\ not (hfull ht)))
//         (ensures (forall v. add v ht = ht' ==> (exists pos. hget pos ht' = Some v)))
// let membership _ _ = ()

val list_left_leaning : #a:eqtype -> l:list a ->
  Lemma (ensures (forall (i:nat) (j:nat).
                 (None? (List.Tot.nth l i) /\ i<j) ==> None? (List.Tot.nth l j)))
let rec list_left_leaning #a = function
    | [] -> ()
    | v::vs -> list_left_leaning vs

open FStar.Classical

val hget_left_leaning : #a:eqtype -> #b:Type -> ht:htree a b ->
  Lemma (requires (hvalid ht))
        (ensures (forall (i:int64{Int64.(i >=^ 0L)}) (j:int64{Int64.(i >=^ 0L)}).
                 (None? (hget i ht) /\ Int64.(i<^j)) ==> None? (hget j ht)))
let hget_left_leaning #a #b ht =
  let aux (i:int64{Int64.(i >=^ 0L)})
          (j:int64{Int64.(i >=^ 0L)})
	  : Lemma (requires (None? (hget i ht) /\ Int64.(i<^j)))
	          (ensures (None? (hget j ht)))
          = if Int64.(j >=^ max_size) then ()
	    else (
	      assert(Int64.(i <^ max_size));
	      list_left_leaning (hto_list ht)
	    )
  in
  let aux' i = move_requires (aux i) in
  forall_intro_2 aux'


// val get_overflow : #a:Type -> h:nat -> (t:tree a) ->
//   Lemma (requires (incremental h t))
//         (ensures (forall (j:nat).
//                  (count_leaves t <= j ==> None? (get j h t))))
// let rec get_overflow h  = function
//   | Empty | Leaf _ -> ()
//   | Node l r ->
//     max_leaves_full (h-1) l;
//     get_overflow (h-1) l;
//     get_overflow (h-1) r


// val left_leaningity : #a:Type -> (ht:htree a) ->
//   Lemma (requires (hvalid ht))
//         (ensures (forall (i:nat) (j:nat).
//                  (None? (hget i ht) /\ i<j) ==> None? (hget j ht)))
// let left_leaningity (_, t) = get_left_leaning default_height t


// /////////// unused stuff

// val valid_leaves : #a:Type -> t:tree a ->
//   Lemma (requires (valid t))
//         (ensures (not (Empty? t) ==> count_leaves t > 0))
//         // /\ has_height h t /\ balanced t))
// let rec valid_leaves = function
//     | Empty | Leaf _ -> ()
//     | Node l r ->
//     valid_leaves l;
//     valid_leaves r

// val left_leaning_children : #a:Type -> t:tree a ->
//     Lemma (requires (left_leaning t))
//           (ensures (Node? t ==> left_leaning (Node?.l t) /\
//                                left_leaning (Node?.r t)))
// let left_leaning_children #a = function
//   | Empty | Leaf _ -> ()
//   | Node l r ->
//     full_is_left_leaning l

// val full_tall : (#a:Type) -> (t:tree a) ->
//     Lemma (requires (full t) /\ (balanced t) /\ (Node? t))
//           (ensures (forall (h:nat).
//                    (has_height (h+1) t) ==>
//                    ((has_height h (Node?.l t)) /\
//                     (has_height h (Node?.r t)))))
// let full_tall (Node l r) = ()

// val simm : #a:Type -> h:nat -> t:tree a -> t':tree a ->
//   Lemma (requires (full t  /\ balanced t  /\ has_height h t /\
//                    full t' /\ balanced t' /\ has_height h t'))
//         (ensures (count_leaves t = count_leaves t'))
// let rec simm h t t' = match t,t' with
//     | Leaf _, Leaf _ -> ()
//     | Node l r, Node l' r' ->
//       simm (h-1) l l';
//       simm (h-1) r r'

// val lemma_leaves_left_leaning : #a:Type -> h:nat -> t:tree a ->
//   Lemma (requires (incremental h t))
//         (ensures (Node? t ==>
//                   count_leaves (Node?.l t) >= count_leaves (Node?.r t)))
// let lemma_leaves_left_leaning h t =
//     match t with
//     | Empty | Leaf _ -> ()
//     | Node l r ->
//            max_leaves (h-1) l;
//            max_leaves (h-1) r;
//            max_leaves_full (h-1) l;
//            max_leaves_full (h-1) r


// // dependent tuple
// type ab = [a:nat & b:nat{b<a}]

// #push-options "--debug --debug_level --detail_errors
// #--hide_uvar_nums --hint_info --log_types --log_queries
// #--print_bound_var_types --print_effect_args --print_full_names
// #--print_implicits --print_universes --prn --silent --timing
// #--trace_error" pop-options

// let rec uncommitted_height (#a:Type) (h:nat) :
// Tot (l:list a {FStar.List.length l=h+1}) =
//   if h = 0 then [Hash.deft]
//   else
//     let rest = uncommitted_height (h - 1) in
//     let h = FStar.List.hd rest in
//     (Hash.hashf h h) :: rest

// val uncommitted : l:list hash {List.Tot.Base.length l = 33}
// let uncommitted =
//     let res = (uncommitted_height 32) in
//     FStar.List.Tot.rev_length res ;
//     FStar.List.Tot.rev res

// let get_root_height tree height : Tot hash =
//   match tree with
//   | Empty -> FStar.List.Tot.index uncommitted height
//   | Node h _ _ -> h
//   | Leaf h -> h

// let rec go_left pos (h:nat{h>=1}): Tot bool (decreases h) =
//   if (h = 1) then
//      (pos `op_Modulus` 2) = 0
//   else go_left (pos/2) (h-1)

// let mask = pow2 (height - 1) in
// UInt32.eq (UInt32.logand pos mask) 0l
