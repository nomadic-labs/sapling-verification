(* The MIT License (MIT)

 Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. *)

From mathcomp.ssreflect
     Require Import ssreflect ssrbool ssrnat eqtype ssrfun seq.

From IMT Require Import prelude commitments.

Export Set Implicit Arguments.
Export Unset Strict Implicit.

(** * Incremental Merkle Trees *)

(** _Incremental Merkle Trees_ are an optimization of full Merkle
trees of fixed height.

A full Merkle tree of height h contains 2 ^ h leaves, and h+1 levels
of nodes, with leaves at level 0 and its root at level h. In the Tezos
codebase, we use IMTs of fixed height (32 in the sapling storage
library for Tezos since the Edo(008) protocol). Trees of fixed size
enable in turn proofs of membership---witnesses---, of fixed size.

Unlike regular Merkle trees, Incremental Merkle Trees can be indeed
[Empty]. Intuitively, an empty IMT models a complete Merkle tree where
all leaves are pre-filled with a default value, given by [uncommitted
0].

IMTs are _incremental_ in the sense that leaves cannot be modified but
only added, and they are moreover inserted exclusively in successive
positions from left to right. IMTs then optimize both the size of the
underlying Merkle tree and the computation of known hashes for default
values:

- Internally, a sub-tree filled with default values is represented by
an [Empty] [tree], avoiding to construct the sub-tree.

- Since all the nodes at the same level of an underlying [Empty]
sub-tree have the same hash, it can be computed from the default value
of the leaves. This is stored in the [uncommitted] list, with index n
> 0.

In the OCaml codebase, an IMT is implemented as a pair [(n,t)] where
[t] is the underlying, optimized Merkle tree, and [n] contains the
size of the tree (which is also the next position to fill). Here, we
will implement IMTs as a Sigma-type (i.e. a dependently-typed record)
made of [tree_of : tree], [size_of : nat], which makes explicit the
specification:

 - [tree_of t \in incremental h]: the underlying tree has an
   [incremental] specification.

 - [size_of t == count_leaves t]: we memoize the number of commitments
   we have already stored (which will also give us the next available
   position in the optimizing implementations).

*)

Module IncrementalTrees.

(** **Internal represention as binary trees *)

Section BaseTree.
  Context (A B: eqType).

(** We define first the internal representation of IMTs. That is, the
underlying implementation of the binary trees, given by the [tree]
datatype. This inductive datatype defines possibly empty binary trees
storing distinct equality-supporting types at their Nodes and
Leaves. *)
Inductive tree : Type :=
  | Empty : tree
  | Leaf (v:A) : tree
  | Node : forall (v:B), tree -> tree -> tree.

Section EqType.

Fixpoint tree_eq (t1 t2 : tree ) :=
  match t1, t2 with
  | Empty, Empty => true
  | Leaf v1, Leaf v2 => v1 == v2
  | Node v1 l1 r1, Node v2 l2 r2 =>
     [&& v1 == v2, tree_eq l1 l2 & tree_eq r1 r2]
  | _ , _  => false
  end.

Lemma tree_eqP : Equality.axiom tree_eq.
Proof.
move; elim=>[|v1| v1 l1 IHL r1 IHR][|v2|v2 l2 r2]; do [by constructor | simpl].
- by case:(v1 =P v2)=>[<-|NE]; [ left | right; case].
case:(v1 =P v2)=>[<-|]/=; last by right; case.
case:(IHL l2)=>/=[<-|]; last by right; case.
case:(IHR r2)=>/=[<-|]; last by right; case.
by constructor.
Qed.

Definition treeT_eqMixin := EqMixin tree_eqP.
Canonical Structure tree_eqType :=
  Eval hnf in EqType tree treeT_eqMixin.
End EqType.
End BaseTree.
Arguments Leaf {A B} v.
Arguments Node {A B} v _ _.
Arguments Empty {A B}.

(** We also add support for ssreflect's \in notation: this will enable
reasoning about whether a commitment is stored in the tree. *)
Section MemPred.
Context (A B :eqType).

Fixpoint mem_tree (t : tree A B) :=
  match t with
    | Empty => xpred0
    | Leaf w => pred1 w
    | Node _ l r => predU (mem_tree l) (mem_tree r)
  end.

Definition tree_eqclass := tree A B.
Identity Coercion tree_of_eqclass : tree_eqclass >-> tree.
Coercion pred_of_tree (t : tree_eqclass) : {pred A} := mem_tree t.

Canonical tree_predType := PredType (pred_of_tree : tree A B -> pred A).

Canonical mem_tree_predType := PredType mem_tree.

Lemma in_leaf x y : (x \in Leaf y) = (x == y).
Proof. by []. Qed.

Lemma in_empty x : (x \in Empty) = false.
Proof. by []. Qed.

Lemma in_node x y l r : (x \in Node y l r) = (x \in l) || (x \in r).
Proof. by []. Qed.
End MemPred.
Notation inT := (in_leaf, in_empty, in_node).

(** ** Incremental Specification *)

(** We define now a theory of Incremental trees: we define the
 different properties that make a tree indeed _incremental_, and prove
 some useful properties and auxiliary lemmas to ease the verification
 effort. We will decompose the [incremental h] specification of a
 [tree] as the conjunction of four small (Boolean) conditions:

 - the tree has a declared fixed height [h], given by the [has_height]
   predicate below,

 - the tree has a [valid] shape,

 - the tree is [left_leaning], i.e. it has been filled from left to
   right,

 - and, the tree is [balanced].

*)

Section Props.
Context {A B: eqType}.
Implicit Type (t : tree A B).


(** IMTs model balanced, full Merkle trees of fixed height, which have
all their leaves at the bottom of the tree, i.e. at [height] 0. Thus
[valid] checks we only put [Empty] constructors in the right place:
either if the tree is really [Empty], or to represent a _cut_ of a
sub-tree full of default values. Thus, we shoud never have [Node _
Empty Empty] values, as this would allow to construct sub-trees with
gaps. *)

Fixpoint valid t : bool :=
  match t with
  | Empty => true
  | Leaf _ => true
  | Node _ l r =>
    [&& l \in valid, r \in valid & negb ((l == Empty) && (r == Empty))]
end.

Lemma validE : Empty \in valid.
Proof. by done. Qed.

Lemma validN x l r:
  Node x l r \in valid  =
      [&& l \in valid, r \in valid & negb ((l == Empty) && (r == Empty))].
Proof. by done. Qed.

(** An IMT is [full] if all the paths from the root store a commitment
at its leaves. *)
Fixpoint full t : bool :=
  match t with
  | Empty => false
  | Leaf _ => true
  | Node _ l r => (l \in full) && (r \in full)
  end.

(** Thus, there is no [Empty] sub-node of a [full] tree. *)
Lemma fullNE t: t \in full -> t != Empty.
Proof. by rewrite -topredE /= /full =>T;case:eqP T=>//->//. Qed.

Lemma fullE: Empty \in full = false.
Proof. by done. Qed.

Lemma fullL v: Leaf v \in full = true.
Proof. by done. Qed.

Lemma fullN x l r:
    Node x l r \in full  = (l \in full) && (r \in  full).
Proof. by done. Qed.

(** All [full] trees are [valid] trees. *)
Lemma full_valid : {subset full <= valid}.
Proof.
elim=>//= w l IL r IR; rewrite fullN /= =>/andP[L R].
rewrite -topredE /= (IL L) (IR R) /=.
by case:l L {IL} ; case:r R {IR}=>//=.
Qed.

(** [count_leaves] gives us the size of the underlying tree. **)
Fixpoint count_leaves t : nat :=
  match t with
  | Empty => 0
  | Leaf _ => 1
  | Node _ l r => count_leaves l + count_leaves r
  end.

Lemma count0 : count_leaves Empty = 0.
Proof. by done. Qed.

Lemma countN b l r:
  count_leaves (Node b l r) = count_leaves l + count_leaves r.
Proof. by done. Qed.

(** [preord] lists the commitments that have been inserted in the
underlying tree. This is an inorder traversal of the binary tree which
does not take forgetting the values stored in Nodes, and hence a
preorder traversal of leaves. **)

Fixpoint preord t : seq A :=
  match t with
  | Empty => [::]
  | Leaf v => [:: v]
  | Node _ l r => preord l ++ preord r
  end.

Lemma preord0 : preord Empty = [::].
Proof. by done. Qed.

Lemma preord1 v: preord (Leaf v) = [:: v].
Proof. by done. Qed.

Lemma preordN x l r: preord (Node x l r) = preord l ++ preord r.
Proof. by done. Qed.

Lemma mem_preord x t : (x \in preord t) = (x \in t).
Proof.
elim:t=>//=[v|v l L r R]; try by rewrite inE inT //.
by rewrite mem_cat L R inT //.
Qed.

Lemma count_size t : count_leaves t = size (preord t).
Proof. by elim:t=>//= x l -> r ->; rewrite size_cat //. Qed.

(** [height] computes the height of underlying binary tree. *)
Fixpoint height t : nat :=
  match t with
  | Empty => 0
  | Leaf _ => 0
  | Node _ l r => (maxn(height l) (height r)).+1
  end.

Lemma heightE : height Empty = 0.
Proof. by done. Qed.

Lemma heightL v : height (Leaf v) = 0.
Proof. by done. Qed.

Lemma heightN v l r:
  height (Node v l r) = (maxn (height l) (height r)).+1.
Proof. by done. Qed.

Lemma heightT t :
  height t = if t isn't (Node _ l r) then 0
             else (maxn (height l) (height r)).+1.
Proof. by case:t=>//. Qed.


(** We have an upper bound of how many commitments we can store: *)
Lemma count_height_bounded t : count_leaves t <= 2 ^ (height t).
Proof.
elim:t=>//= v l /leq_add IH r /IH{}IH.
apply:(leq_trans IH).
rewrite expnS mul2n -addnn; rewrite /maxn.
by case:ltnP=>[/ltnW|]L; rewrite ?leq_add2r ?leq_add2l leq_exp2l //.
Qed.

(** The [has_height] predicate binds the fact that the [h] argument in
the specification should prescribe the height of the IMT. Since we
overload the [Empty] constructor to also denote the optimization of a
subtree full of pre-defined values, we will allow [Empty] trees to
"declare" any height. Thus, [has_height] is indeed an upper bound on
the height of the tree. **)
Definition has_height n : pred (tree A B) :=
  fun t => (t == Empty) || (height t == n).

Lemma has_heightE n : Empty \in has_height n.
Proof. by done. Qed.

Lemma has_heightT t : t \in has_height (height t).
Proof. rewrite -!topredE /has_height /= eq_refl orbT //. Qed.

Lemma has_height0E t :
  t \in has_height 0 -> (t = Empty \/ exists w, t = Leaf w).
Proof. by case:t=>//[|w]_; by [ left | right; eauto]. Qed.

Lemma has_heightNE t h:
  t != Empty -> (t \in has_height h) = (height t == h).
Proof. move=>/negbTE H; rewrite -topredE /has_height /= H //. Qed.

Lemma has_heightN w l r h :
  (Node w l r) \in has_height h = (h == (maxn (height l) (height r)).+1).
Proof. rewrite -!topredE /has_height //=. Qed.

(** As stated above, [has_height] is an upper bound of the tree's [height]. *)
Lemma has_height_bounded h:
  {in has_height h, forall t, height t <= h}.
Proof.
by move=>t; rewrite -topredE /has_height /=;  case/orP=>[/eqP->//|/eqP<-]//.
Qed.

(** As a corollary, [has_height h] also makes [2 ^ h] an upper bound
on how many commitments are stored in the tree. *)
Lemma count_has_height_bounded h:
  {in has_height h, forall t, count_leaves t <= 2 ^ h}.
Proof.
move=>t /has_height_bounded H.
apply:(leq_trans (count_height_bounded t)).
by rewrite leq_exp2l //.
Qed.

(** As we mentioned above, Incremental Merkle Trees are filled
_incrmentally_ from left to right, without gaps. That is, without any
[Empty] subtree in between (Leaves) at the same height. We encode this
requirement with the [left_leaning] predicate. *)

Fixpoint left_leaning t : bool :=
  match t with
  | Empty => true
  | Leaf _ => true
  | Node _ l Empty =>  l \in left_leaning
  | Node _ l r => (l \in full) && (r \in left_leaning)
  end.

Lemma left_leaningE : Empty \in left_leaning.
Proof. by done. Qed.

Lemma left_leaningN x l r:
  Node x l r \in left_leaning =
      (r \in left_leaning) &&
         (if r is Empty then l \in left_leaning else l \in full).
Proof.
rewrite -!topredE /=.
by case:r=>//=[w|w r1 r2]; rewrite -!topredE ?andbT //= andbC //.
Qed.

(** [full] trees are indeed [left_leaning]: *)
Lemma full_left_leaning : {subset full <= left_leaning}.
Proof.
elim=>//= w l IHL r IHR; rewrite fullN =>/andP [FL /IHR{}IHR].
rewrite left_leaningN {}IHR /=.
by case:r=>//; exact:(IHL FL).
Qed.

(** However, [left_leaning] is not enough to reason about incrementality
and we still need a separate [valid] predicate, as the following lemma
is not necessarily true: *)

Lemma left_leaning_valid : {subset left_leaning <= valid}.
Proof.
Abort.
(** Why? Mostly because we take [Empty] to overload both *really*
Empty and filled with default values, so the induction hypothesis
cannot syntactically distinguish between those. *)

(** In the proofs below, we want to case on whether a tree is full or
not. So we pack this nicely: *)
Lemma fullP t : decidable (t \in full).
Proof.
elim:t;[by right | by left|move=>w l L r R].
rewrite fullN; case:L=>[->/=|/negP/negbTE->/=]; last by right.
by case:R=>[->//|/negP/negbTE->/=]; [ left | right].
Qed.

(** IMTs model complete, balanced Merkle trees. However, since they do
so by overloading [Empty] to denote _cuts_, we need a tailored version
of the traditional definition of a [balanced] binary tree which
accounts for the left-leaning skew. *)

Fixpoint balanced t :=
  if t isn't (Node _ l r) then true
  else if l is Empty then r \in balanced
       else if r is Empty then l \in balanced
            else [&& height l == height r,
                     l \in balanced & r \in balanced].

Lemma balancedE : Empty \in balanced.
Proof. by done. Qed.

Lemma balancedN w l r:
    Node w l r \in balanced =
         [&&  l \in balanced, r \in balanced &
             (l != Empty) && (r != Empty) ==> (height l == height r)].
Proof.
have [IL IR]: Node w l r \in balanced <->
           [&& l \in balanced, r \in balanced&
             (l != Empty) && (r != Empty) ==> (height l == height r)].
- split.
 + rewrite -!topredE /=.
  case:l=>//=[|v|v l1 l2];first by rewrite andbT.
  - by case:r=>//=.
  rewrite -!topredE /=; case:r=>//=[->//|z r1 r2]; case/and3P=>/= -> -> -> //.
 case/and3P=>/= L R; rewrite -topredE /= {}L {}R /= andbT.
 by case:l=>//=[v|v l1 l2]; case:r=>//=.
set b := Node w l r \in balanced in IL IR *.
set c := [&& _, _ & _] in IL IR *.
apply:(introTF (P:=b))=>//=; first by apply:idP.
by case:ifP=>// /negbT /(contra IL) /negP //.
Qed.

(** [max_leaves] denotes the maximum capacity of the IMT, that is 2 ^
[height] t. *)
Definition max_leaves t : bool := count_leaves t == 2 ^ (height t).

(** Balancing lemma: If t is an [balanced] tree, then [t \in full]
entails that the tree t has [max_leaves].  **)
Lemma balanced_full_max :
       {in balanced, {subset full <= max_leaves}}.
Proof.
elim=>//= w l IL r IR.
rewrite -!topredE /max_leaves //= in IL IR *.
rewrite -!topredE /=.
case:l IL=>//=[x|x l1 l2]; first by case:r {IR}=>//= y r1 r2; rewrite andbF //.
case:r IR=>//=; rewrite ?andbF //.
move=>/= z r1 r2 IHR IHL.
rewrite - !andbA =>/and3P[/eqP HH IL  IR] /and4P[L1 L2 R1 R2].
(* I need to fiddle a bit for these proofs. I could find the right
combo of rewriting with andbA,andbC,andbCA, etc. but this is faster *)
have ->: count_leaves l1 + count_leaves l2
           = 2 ^ (maxn (height l1) (height l2)).+1.
-  move:IHL; rewrite L1 L2 /= =>/(_ IL isT)/eqP->//.
have ->: count_leaves r1 + count_leaves r2
           = 2 ^ (maxn (height r1) (height r2)).+1.
- by move:IHR; rewrite R1 R2 /= =>/(_ IR isT)/eqP->//.
(* Now some basic math *)
by rewrite HH maxnn addnn -muln2 mulnC //=.
Qed.

(** Converselly, [valid] and [balanced] trees which are not [full]
have strictly less leaves than the maximum capacity. *)
Lemma balanced_not_full_not_max:
  {in (predI valid balanced), forall t,
        t \notin full -> count_leaves t < 2 ^ height t}.
Proof.
elim=>//= w l IL r IR; rewrite fullN.
case:l IL=>//=[_ _ _|x _ |x l1 l2]; rewrite ?add0n ?max0n /=.
- by apply:(leq_ltn_trans (count_height_bounded r)); rewrite ltn_exp2l //.
- by case:r IR =>//z r1 r2 /= _ /=; rewrite inE /= andbF //.
move=>IHL; rewrite inE /= andbT.
case:r IR=>//=[ _ _ _|z _|z r1 r2]; rewrite ?addn0 ?maxn0 ?andbF //=.
- apply:(leq_ltn_trans (count_height_bounded (Node x l1 l2))).
 by rewrite ltn_exp2l //.
move=>IHR /and4P[] /andP[VL VR] /eqP E IL IR.
have : (Node x l1 l2 \in predI valid balanced) &&
       (Node z r1 r2 \in predI valid balanced).
- by move:VL VR IL IR; rewrite !inE -!topredE/= =>-> -> -> ->//.
case/andP=>/IHL{}IHL /IHR{}IHR; case/nandP=>[/IHL{}IHL|/IHR{}IHR].
- case:(fullP (Node x r1 r2))=>[|/negP/IHR]; rewrite -!E maxnn.
 + move/(balanced_full_max IR); rewrite -topredE /max_leaves /= =>/eqP->.
  by rewrite -E [in y in _ < y]expnS mul2n -addnn ltn_add2r IHL //.
 rewrite -(ltn_add2l (count_leaves l1 + count_leaves l2))=>HL.
 apply:(ltn_trans HL).
 by rewrite [in y in _ < y]expnS mul2n -addnn ltn_add2r IHL //.
(* The dual case *)
case:(fullP (Node x l1 l2))=>[|/negP/IHL]; rewrite !E maxnn.
- move/(balanced_full_max IL); rewrite -topredE /max_leaves /= =>/eqP->.
 by rewrite E [in y in _ < y]expnS mul2n -addnn ltn_add2l IHR //.
rewrite -(ltn_add2r (count_leaves r1 + count_leaves r2))=>HR.
apply:(ltn_trans HR).
by rewrite [in y in _ < y]expnS mul2n -addnn ltn_add2l IHR //.
Qed.

(** Finally, we pack the four properties that make a [t :tree] a
proper Incremental Merkle tree of height [h]: *)

Definition incremental h t : bool :=
  [&& t \in valid, t \in left_leaning ,
                         t \in balanced & t \in has_height h].


(* TODO: Add hashing properties for nodes! *)

Lemma incrementalE h : Empty \in (incremental h).
Proof. by rewrite /incremental //. Qed.

Lemma incrementalL h v :
  Leaf v \in (incremental h) = (h == 0).
Proof. by rewrite /incremental /has_height -!topredE /= eq_sym //. Qed.

Lemma incremental0 t :
  t \in (incremental 0) <-> t = Empty \/ exists v, t = Leaf v.
Proof.
rewrite /incremental /has_height -!topredE /=.
split; last by case=>[|[v]]-> //.
by case/and4P=>// _ _ _; case/has_height0E; [left | right; eauto].
Qed.

(** Weakening lemma for [incremental] children of an
[incremental] Node. *)
Lemma incrementalN h w l r:
  Node w l r \in (incremental h) ->
        (l \in incremental h.-1) && (r \in incremental h.-1).
Proof.
rewrite /incremental -!topredE /= left_leaningN balancedN /= .
rewrite has_heightN implybE negb_and !negbK -topredE /= -!orbA.
case/and4P=>/and3P[L R NE]/andP[RI]RE.
have LI: l \in left_leaning.
- by case:r RE{RI R}NE=>//=[v|v r1 r2]/= /full_left_leaning =>//.
case/and3P=>LB RB O3 /eqP -> /={RE}.
rewrite L R RI LI LB RB /=.
move:O3; case/or3P=>/=[/eqP->|/eqP->|/eqP HH]/=; rewrite ?max0n ?has_heightT //.
by rewrite -{1}HH [in a in _&& a]HH !maxnn !has_heightT //.
Qed.

(** A special Weakening lemma for children of incremental trees used
in the inductive cases of [insert] below: if the left subtre [l \notin
full], then [r] must necessarily be [Empty]. *)
Lemma incremental_NfullRE w l r h:
  Node w l r \in incremental h -> l \notin full -> r = Empty.
Proof.
by case/and4P; rewrite left_leaningN=> _ /andP[_]; case:r=>//= [z|z l2 r2]->//.
Qed.

(** We wrap the [balanced_full_max], [count_height_bounded], and
[balanced_not_full_not_max] lemmas above, which relate the height and
the leaves count with the incremental spec, to avoid opening the
[incremental h] hypothesis. *)

(* NB: this one is called max_leaves in FStar. TODO: Unify names *)
Lemma incremental_leaves_bounded h:
      {in incremental h, forall t, count_leaves t <= 2 ^ h}.
Proof. by move=>t /and4P[_ _ _] /count_has_height_bounded //. Qed.

Lemma incremental_Nfull_strict_height h:
  {in incremental h, forall t,
        t \notin full -> count_leaves t < 2 ^ height t}.
Proof.
move=>t /and4P[V C I];rewrite /has_height -topredE.
case/orP=>[/eqP->//=|_ NF].
have: t \in predI valid balanced.
 - by move: V C I; rewrite -!topredE /= =>-> _ ->//.
by move/balanced_not_full_not_max/(_ NF).
Qed.

Lemma incremental_Nfull_strict_bounded h:
  {in incremental h, forall t,
        t \notin full -> count_leaves t < 2 ^ h}.
Proof.
move=>t IS /(incremental_Nfull_strict_height IS).
case/and4P: IS=> _ _ _ /has_height_bounded.
rewrite leq_eqVlt; case/orP=>[/eqP ->//|H N].
by apply:(ltn_trans N); rewrite ltn_exp2l //.
Qed.

Lemma incremental_full_max_leaves h:
  {in incremental h, full =i max_leaves}.
Proof.
move=>t /and4P[V _ T _].
case:(fullP t)=>[F|/negP F]; rewrite ?F.
- by rewrite (balanced_full_max T F) //.
move:(F)=>/negbTE ->//.
have: t \in predI valid balanced by move:V T; rewrite -!topredE /= =>-> ->//.
- move/balanced_not_full_not_max/( _ F).
by rewrite -topredE /max_leaves /=; case:eqP=>// ->//; rewrite ltnn //.
Qed.

Lemma incremental_full_height h:
   {in incremental h, forall t, t \in full -> height t == h}.
Proof. by move=>t /and4P[_ _ _]H /fullNE /has_heightNE <- //. Qed.

(* The next one is called max_leaves_full in FStar:

 val max_leaves_full : #a:Type -> h:nat -> t:tree a ->

   Lemma (requires (valid t /\ is_tall h t /\ balanced t))

   (ensures (leaves t = pow2 h <==> full t))

TODO: Unify names!
*)
Lemma incremental_full_leaves h:
  {in incremental h, forall t, t \in full = (count_leaves t == 2 ^ h)}.
Proof.
move=>t IS; case:(fullP t)=>[|/negP F].
- move/(incremental_full_height IS)/eqP=><-.
 by rewrite (incremental_full_max_leaves IS) /max_leaves -topredE //.
(* Otherwise I reach a contradiction *)
rewrite (negbTE F); case:eqP=>//E.
move:(incremental_Nfull_strict_bounded IS F).
by rewrite E ltn_exp2l // ltnn //.
Qed.

(* TODO So far we have mentioned that IMTs model Merkle trees, but we
 have not proved that. We could prove, for instance, the equivalence
 between the trees underlying an IMT to a full, balanced, Merkle trees
 with a default leave to mark Empty slots. *)
End Props.

(** ** Packing Incremental Merkle Trees as Sigma Types *)

(** The external API of an _Incremental Merkle Tree_ in the Ocaml
implementation is defined over a pair of a [tree] of fixed size (the
default size is 32) and a natural number [n] which keeps track of the
size of the tree. That is, [n] records how many commitments have been
inserted in the tree, allowing us to quickly determine whether it is
full or not. However, this invariant is not enforced by the type
signature.*)

Section IMTs.
Context {A B: eqType}.

(** Here, we will take advantage of the dependently typed setting to
implement the abstract data-type as a _small_ Sigma-type with an
argument denoting the height. In addition to the pair of components,
[tree_of : tree A B], and [size_of : nat], we add to the dependent
record type two (Boolean, hence the "small" above) conditions: (i)
that the underlying carrier tree, [tree_of], is indeed an Incremental
Merkle Tree [ t \in incremental height], and (ii), that [size_of]
effectively memoizes the size of the tree. *)

Structure IMT (h : nat) :=
  MakeIMT { tree_of : tree A B;
            size_of : nat ;
            _ : tree_of \in incremental h;
            _ : count_leaves tree_of == size_of
          }.

(** NB: By the [left_leaning] nature of [incremental] trees, the
[size_of] argument also denotes the next position in the tree
available for insertion. *)

Section EqType.
Variable (h : nat).

(** Notice that the definition of [IMT]s above entails that we need
only compare the underlying trees in order to determine the equality
of two IMTs, as equal carriers necessarily require equal sizes (and
equal height by typing). *)
Definition imt_eq (t1 t2 : IMT h) := tree_of t1 == tree_of t2.

Lemma imt_eqP : Equality.axiom imt_eq.
Proof.
case=>t1 n1 I1 L1 [t2 n2 I2 L2].
rewrite /imt_eq /= ; case:(t1 =P t2)=>E; last by constructor; case=>/E//.
(* t1 = t2 case only needs to lift equality through boolean proofs *)
have N : n1 = n2 by rewrite -(eqP L1) -(eqP L2) E //.
rewrite -E -N /= in L2 I2 *.
rewrite (bool_irrelevance L1 L2) (bool_irrelevance I1 I2).
by constructor.
Qed.

Definition imt_eqMixin := EqMixin imt_eqP.
Canonical Structure IMT_eqType :=
  Eval hnf in EqType (IMT h) imt_eqMixin.
End EqType.

Section Props.
Variable (h : nat).
Implicit Type t : IMT h.

(** We lift some lemmas from the underlying internal representation of
IMTs to the packed datatype: *)

Lemma imt_spec_incremental t : (tree_of t) \in incremental h.
Proof. by case:t=>//. Qed.

Lemma imt_spec_count t: count_leaves (tree_of t) = size_of t.
Proof. by case:t=>/= t1 s1 _ /eqP ->//. Qed.

Lemma imt_spec_max_leaves t: (size_of t) <= 2 ^ h.
Proof.
case:t=>/= t1 s1 IS /eqP <-.
exact:(incremental_leaves_bounded IS).
Qed.

(** The [size_of] argument reflects when the carrier tree is [full].*)
Lemma imt_spec_full t: reflect ((size_of t) = 2 ^ h) ((tree_of t) \in full).
case:t=>/= t1 s1 I L; rewrite -(eqP L).
case:(fullP t1)=>[F|/negP F]; rewrite ?F ?(negbTE F); constructor.
- by move:(F); rewrite (incremental_full_leaves I) =>/eqP ->//.
(* if t1 \notin full, then we have must have s1 < 2 ^h *)
move=>E; move:(incremental_Nfull_strict_bounded I F).
by rewrite E ltnn //.
Qed.
End Props.
End IMTs.

(** ** Canonical API  *)

(** We provide a canonical, reference implementation of the API for
IMTs. We will prove these reference functions satisfies the desired
specification, and then we will use as a model for verifying the
correctness of the concrete implementations against this model. *)
Section Canonical.
Notation A := commitments.Commitments.C.
Notation H := commitments.Commitments.H.
Notation tree := (@tree commit_eqType hash_eqType).
Notation xo := (hash_to_commitment (uncommitted 0)).

(** *** Canonical Internal API *)

(** We give a refernece implementation of the internal API defined in
terms of [insert] and [get] over [t : tree]. *)
Section InternalAPI.
(* [get] the [pos]-placed element from the left *)
Fixpoint get (pos height : nat) (t : tree) : option A :=
  match t with
  | Empty => None
  | Leaf v => if pos == 0 then Some v else None
  | Node w l r =>
    if pos < count_leaves l then
      (* we look on the left *)
      get pos height.-1 l
    else get (pos - (count_leaves l)) height.-1 r
  end.

(** Getting an element of the tree by position corresponds to getting
the nth element from the [preord]er of the tree. **)
Lemma get_preorderE p h t:
       get p h t = if p < count_leaves t then Some (nth xo (preord t) p)
                    else None.
Proof.
elim:t p h=>//=[w|w l IHL r IHR]p h.
- by rewrite ltnS leqn0 /=; case:ifP=>//= /eqP ->//.
rewrite IHL IHR nth_cat -count_size.
by case:ltnP=>[/ltn_addr ->|/ltn_subLR->]//.
Qed.

Lemma get_none pos h t:
  count_leaves t <= pos -> get pos h t = None.
Proof. by rewrite get_preorderE leqNgt =>/negbTE ->//. Qed.

Lemma get_some pos h t:
   pos < count_leaves t -> get pos h t = Some (nth xo (preord t) pos).
Proof. by rewrite get_preorderE=>->//. Qed.

(** We use the two properties above to build a "co-inductive
specification" for membership, and a reflection lemma between get and
boolean membership. Thus, we hide the existential quantification for
the position of the element. *)

Variant tree_get_spec x h t : bool -> Type :=
| tree_get_some p :
     p < count_leaves t -> p = index x (preord t) ->
     get p h t = Some x -> tree_get_spec x h t true
 | tree_get_none p:
     count_leaves t <= p <= index x (preord t) ->
     get p h t = None -> tree_get_spec x h t false.

(** [getP] If x is in the tree, then it is the nth element and we can get it *)
Lemma getP x h t : tree_get_spec x h t (x \in t).
Proof.
elim:t h=>//[|w|w l IL r IR]h; rewrite ?inT; first by apply:tree_get_none=>//=.
- case:eqP=>//[->|/nesym/eqP/negbTE W].
 + by apply:tree_get_some=>//=; rewrite eq_refl //.
 apply:(tree_get_none (p:=1))=>//=; rewrite W //.
case:(IL h.-1)=>//=[p C P F|p1 /andP[]/leq_trans H1/H1{}H1 N1].
- by apply:tree_get_some=>//=;
  rewrite index_cat -index_mem -count_size -P !C ?ltn_addr // F //.
have /negbTE L: x \notin (preord l).
- rewrite -index_mem ltnNge -count_size H1 //.
  case:(IR h.-1)=>//=[p C P F|p2 /andP[C2 R2] N2]; last first.
- apply:(tree_get_none (p:=count_leaves l +p2))=>//=.
 + by rewrite index_cat L !leq_add // count_size //.
 rewrite ltnNge leq_addr /= get_preorderE -addnBAC //.
 by rewrite subnn add0n ltnNge //= C2 //.
apply:tree_get_some=>//=; rewrite index_cat L /= -count_size -P.
- by rewrite ltn_add2l C //.
by rewrite ltnNge leq_addr /= -addnBAC // subnn add0n F //.
Qed.

(** [find] a commit by its value. *)
Definition find x h t := get (index x (preord t)) h t.

Variant tree_find_spec x h t : bool -> Type :=
| tree_find_some:
     find x h t = Some x -> tree_find_spec x h t true
 | tree_find_none:
     find x h t = None -> tree_find_spec x h t false.

Lemma findP x h t : tree_find_spec x h t (x \in t).
Proof.
case:(getP x h t)=>//[p I H X|p /andP[]/leq_trans H1/H1{}H1 X].
- by apply:tree_find_some; rewrite /find -H X //.
by apply:tree_find_none; rewrite /find get_preorderE ltnNge H1 //.
Qed.

(* TODO: are we using find? Should we keep it *)

(** [get_root_height] projects the root hash at a certain [height]. It
will be used by the different implementations of insert to re-compute
the hashes at each Node level. Notice that when we deal with an
[Empty] subtree, we use the height to recover the corresponding hash
from the [uncommitted] list. *)
Definition get_root_height tree height : H :=
  match tree with
  | Empty => uncommitted height
  | Leaf v => hash_of_commitment v
  | Node w  _ _ => w
  end.

(** [insert_model] inserts an element [v] at the first available
position of the tree. Notice that, unlike the implementation in OCaml,
it does not rely on the position of the element but rather checks
whether the left subtree is full or not. *)

Fixpoint insert_model' (v:A) (t:tree) (h:nat) : tree :=
  match t with
       | Empty => if h isn't (S h') then Leaf v
                  else let t1 := insert_model' v Empty h'
                       in let w := merkle_hash h'
                               (get_root_height t1 h')
                               (get_root_height Empty h')
                          in Node w t1 Empty
       | Node cm tl tr =>
         (* The match on h is required to guide the termination checker *)
         if h isn't (S h') then Node cm tl tr
         else if (tl \in full)
              then let t1 := insert_model' v tr h'
                   in let w := merkle_hash h'
                                      (get_root_height tl h')
                                      (get_root_height t1 h')
                      in Node w tl t1
              else let t1 := insert_model' v tl h'
                   in let w := merkle_hash h'
                                           (get_root_height t1 h')
                                           (get_root_height tr h')
                      in Node w t1 tr
       | Leaf w =>
         (* not possible if the ~ full precondition holds *)
         Leaf w
  end.

(** Specification of [insert_model]: inserting a commitment [v] on
non-[full], [incremental] tree [t] results in an incremental tree
[res], extends [t] with [v] in the next available position
([count_leaves t]). *)

Definition insert_model_pre (t:tree) h :=
   [/\ t \in incremental h & t \notin full ].

Definition insert_post (v:A) (t:tree) h res : Prop :=
  [/\ res \in incremental h,
              count_leaves res = (count_leaves t).+1 &
              get (count_leaves t) h res = Some v].

(** Notice that the precondition of [insert] entails that
[insert_model'] is not supposed to be called on full trees. Thus we
wrap [insert_model'] to return the tree if it is called on an already
full tree to make the function total. **)

Definition insert_model v t h :=
  if t \in full then t else insert_model' v t h.

Lemma insert_modelF v t h : t \in full -> insert_model v t h = t.
Proof. by rewrite /insert_model=>->//. Qed.

Lemma insert_modelPNE v l n: (insert_model' v l n != Empty).
Proof.
case:l=>//=[|w| w l r]//; case:n=>//= n.
by case:ifP=>//.
Qed.

Lemma eq_insertF v x t h :
   insert_model v t h \in full = (insert_model x t h \in full).
Proof.
rewrite /insert_model; case:(fullP t)=>[|/negP]F; rewrite ?F ?(negbTE F) //.
elim:h t v x F=>[[]//|/= n IH]; case=>//[|w l r]v x F.
- by rewrite !fullN (IH _ v x F) //.
rewrite fullN negb_and in F.
by case:(fullP l) F=>[|/negP]L;
   rewrite ?L ?(negbTE L) !fullN /= => R ;
   [rewrite (IH r v x R) // | rewrite (IH l v x L) //].
Qed.

(** The initial tree will in practice be computed only once: the first
time we insert a commitment to an [empty] IMT. We fork its definition
from the corresponfing case of [insert_model], in order to derive
better lemmas for it. Notice that in the OCaml implementations, when
we trigger a call to create the initial tree, we ignore the position
where we insert, as we will always insert in the 0 [leftmost]
position. *)

Fixpoint initial_tree (v:A) (h: nat) :=
  if h isn't (S h') then Leaf v
  else let t1 := initial_tree v h'
       in let w := merkle_hash h'
                               (get_root_height t1 h')
                               (get_root_height Empty h')
          in Node w t1 Empty.

(** Some lemmas about the [initial_tree] to ease the proofs of the
specification below. *)
Lemma insert_modelE v h :
    insert_model' v Empty h = initial_tree v h.
Proof. by elim:h=>//= n <- //. Qed.

Lemma insert_modelL v w h : insert_model' v (Leaf w) h = Leaf w.
Proof. case:h=>//. Qed.

Lemma initial_preord v h : preord (initial_tree v h) = [:: v].
Proof. by elim:h=>// n /= -> //. Qed.

Lemma initial_leaves v h : count_leaves (initial_tree v h) = 1.
Proof. by rewrite count_size initial_preord //. Qed.

Lemma initial_treeF v h :
  initial_tree v h \in full = (h == 0).
Proof. by case:h=>//= n; rewrite fullN fullE andbF //=. Qed.

Lemma initial_incremental v h : (initial_tree v h) \in incremental h.
Proof.
elim:h=>//= n; rewrite -topredE /incremental //=.
set t:= initial_tree v n; set w:= merkle_hash _ _ _.
case/and4P=>V I B H.
rewrite -topredE /= left_leaningN left_leaningE validN validE /=.
rewrite balancedN has_heightN maxn0 eq_refl andbT andbF V I B /=.
have T: t != Empty by rewrite /t; case:n {t w V I B H}=>//.
rewrite (has_heightNE _ T) in H.
by rewrite T (eqP H) eq_refl //.
Qed.

(** In order to prove that [insert_model] satisifies the
specification, we will prove two auxiliary lemmas: (i) that
[insert_model] preserves the [incremental] specification, and (ii),
that the inserted element is in the next available position, i.e. that
it is appendred at the tal of the commitment preorder. *)

(** First, we prove the monotonicity of the [incremental] specification
w.r.t. [insert_model]: **)
Lemma insert_model_incremental v t h :
   t \in incremental h ->
         (insert_model v t h) \in incremental h.
Proof.
move=>T; rewrite /insert_model; case:ifP=>//= /negbT F.
elim:h t T F=>/=[t|n IH]; first by case/incremental0 =>[->//|[w]->]//.
case=>[_|w|w l r].
- (* [Empty] case *)
 move:(insert_modelPNE v Empty n)=>NE.
 move/IH=>/(_ (incrementalE n))=>/= /and4P[V I B].
 rewrite (has_heightNE _ NE) =>/eqP X.
 rewrite /incremental -topredE /= validN balancedN left_leaningN //=.
 by rewrite has_heightN /= I V B negb_and NE /= maxn0 X eq_refl //.
- (* [Leaf w] case *) by rewrite incrementalL //.
(* [Node w l r] case *)
rewrite fullN =>T; case/andP:(incrementalN T)=>/= L R.
case/and4P:(T); rewrite validN balancedN has_heightN.
case/and3P=>VL VR FE _ /and3P[LI LR HH] /= /eqP/succn_inj N.
case:ifP=>/=[LF/(IH _ R)|/negbT LF _]; last first.
(* if [l \notin full], then [r] must necessarily be Empty *)
- move:(incremental_NfullRE T LF)(IH _ L LF)=>RE.
 rewrite RE /= andbF implyFb maxn0 eq_refl andbT in FE HH N *.
 move:(insert_modelPNE v l n); set t:= insert_model' v l n=>NE.
 case/and4P=>[VT IT BT]; rewrite (has_heightNE _ NE)=>/eqP H.
 rewrite /incremental -topredE /=.
 rewrite validN left_leaningN balancedN has_heightN.
 by rewrite validE VT eq_refl andbT IT BT !NE H maxn0 //=.
(* [l \in full], I descend on the right subtree *)
have LE: (l!= Empty) by case:eqP LF=>// ->//.
move:(insert_modelPNE v r n); set t:= insert_model' v r n=>NE.
case/and4P=>VT IT BT H.
rewrite -topredE /incremental /= validN VL {}VT /=.
rewrite balancedN has_heightN left_leaningN LI {}IT {}BT insert_modelPNE /=.
rewrite negb_and NE orbT andbT /=.
set TL := (match t with Empty => _ | _ => _ end).
have -> /= : TL = true by rewrite /TL; case:t NE H TL =>//.
rewrite LE implyTb /= N {TL} in HH *.
rewrite /t in NE H *.
by case:(r =P Empty)N HH NE H=>/=[->/=|_ -> /eqP ->];
  [ rewrite maxn0 =><- _ /has_heightNE -> /eqP -> |
    rewrite maxnn=> /has_heightNE -> /eqP ->];
    by rewrite !maxnn !eq_refl //.
Qed.

(** Then, we prove that inserting into a tree [t], corresponds to
appending to the tail of the commitments preorder. This lemma relies
on the fact that [incrmental] trees have no gaps.*)
Lemma insert_model_preord v t h:
  t \in incremental h -> t \notin full ->
        preord (insert_model v t h) = rcons (preord t) v.
Proof.
move=>T; rewrite /insert_model; case:ifP=>//= /negbT F _.
elim:h t T F=>/=[t|n IH]; first by case/incremental0 =>[->//|[w]->]//.
case=>[_ /=|w|w l r]; first by rewrite insert_modelE initial_preord cats0 //.
- by rewrite incrementalL //.
rewrite fullN=>T; move:(incrementalN T)(T)=>/=/andP[]/IH IL /IH IR {IH}.
move/incremental_NfullRE => RE.
case:ifP=>//=[_ /IR ->|/negbT L _]; first by rewrite rcons_cat //.
by rewrite (RE L) (IL L) /= !cats0 //.
Qed.


(** Now, we cobine the two lemmas above to prove that [insert_model]
satisifies the specification. *)
Theorem insert_model_has_spec v t h :
  insert_model_pre t h ->
         insert_post v t h (insert_model v t h).
Proof.
case=>T F.
move:(insert_model_incremental v T)(insert_model_preord v T F)=>I P.
rewrite /insert_post get_preorderE 2!count_size P size_rcons ltnS leqnn.
by rewrite nth_rcons ltnn eq_refl I //.
Qed.


(** [iter_insert] takes a list of commitments and batches their
insert, one after the other. **)

Definition iter_insert vs t h := foldl (fun t v => insert_model v t h)
  t vs.

(** The precondition for the iterated insert states that, as in the
OCaml implementation, we are not going to insert the list of
commitments partially. Thus, we require that we have enough space in
the tree for the whole sequence. *)

Definition iter_insert_pre (t : tree) h (vs : seq A) := [ /\ t \in
  incremental h & count_leaves t + size vs <= 2 ^ h ].

(** The post-condition of iterated insertion, [iter_insert_model]
states that the result [res] is an [incremental] tree whose size
reflects adding the full batch of commitments [vs]. Moreover, the
statement about the place of the inserted commitment has been
generalized to show that for [ j \in {0 .. count_leaves t}], we [get]
the commitments previously stored in the tree---this is after all and
add-only, data structure. For those commitments in the subsequent
[(size vs)] positions, these are inserted respecting the order in the
list, consecutively in the next available positions. *)

Definition iter_insert_post vs (t0:tree) h res : Prop :=
  [/\ res \in incremental h,
             count_leaves res = (count_leaves t0) + size vs &
             forall j, get j h res =
                  if count_leaves t0 <= j < count_leaves t0 + size vs
                  then Some (nth xo vs (j - count_leaves t0))
                  else get j h t0].

(** NB: Notice that the last clause also entails that there are no
commitments stored at overflowing position, i.e. that if [j >
count_leaves t0 + size vs] then [get j h res = None], as expected. *)

(* TODO Should I add an stating the obvious lemmas about the
post-condition e.g. that it preserves the order of commitments in
pv[s? *)

(* TODO Ditto for showing that if vs = [v] then both
[insert_model_post] and [iter_insert_post] are equivalent. *)

Lemma iter_insertF t h vs: t \in full -> iter_insert vs t h = t.
Proof. by elim:vs t h=>//= v vs IH t h F; rewrite insert_modelF // IH //. Qed.

Lemma iter_insert0 t h : iter_insert [::] t h = t.
Proof. by done. Qed.

Lemma iter_insertC v vs t h :
  iter_insert (v :: vs) t h = iter_insert vs (insert_model v t h) h.
Proof. by done. Qed.

(** Again, in order to prove the specification of iterated insertion,
we lift the two intermediate lemmas we proven for [insert_model]
(monotonicity of the incremental spec, and the shape of the resulting
preorder) to the iterated case *)

(** Monotonicity is trivial, as it will follow straight by induction
on the list, and the fact that singleton insertion with [iter_insert]
preserves the [incremental] property. *)
Lemma iter_insert_incremental vs t h:
  t \in incremental h -> iter_insert vs t h \in incremental h.
Proof.
elim:vs t h=>//= v vs IH t h IS.
case:(fullP t)=>[|/negP]F.
- by rewrite (insert_modelF _ _ F) (iter_insertF _ _ F) //.
(* it suffices to lift the fact that insert preserves the
[incremental] specification with the IH. *)
by exact: (IH _ _ (insert_model_incremental v IS)).
Qed.

(** For the preorder property, there will be a subtle difference: now
we need to consider that we might fill the tree in the middle of the
iteration, and thus we need to reflect which elements of [vs] are
discarded: *)
Lemma iter_insert_preorder vs t h:
  t \in incremental h ->
        preord (iter_insert vs t h ) =
           if (count_leaves t + size vs) <= 2^h
           then preord t ++ vs
           else preord t ++ take (2^h - count_leaves t) vs.
Proof.
elim:vs t h=>[|/= v vs IH] t h IS; first by rewrite /= cats0 if_same //.
case:(fullP t)=>[|/negP]F; rewrite ?F ?(negbTE F).
- move:(F); rewrite (insert_modelF _ _ F) (iter_insertF _ _ F).
  rewrite (incremental_full_leaves IS)=>/eqP ->.
  by rewrite -[in y in _ <= y](addn0 (2^h)) leq_add2l ltn0 subnn // cats0 //.
(* the first insertion satisfies the spec *)
have: insert_model_pre t h by rewrite /insert_model_pre IS F //.
case/(insert_model_has_spec v)=> WIS LV _.
rewrite (IH _ _ WIS) LV (insert_model_preord _ IS F) -cats1.
rewrite addSn addnS -!catA subnS; case:ifP=>P; first by rewrite cat1s.
(* It suffices to show [n] cannot be 0, as [ t \notin full] *)
set n := 2 ^h - _; suff -> /=: n = n.-1.+1 by rewrite cat1s //.
by rewrite /n prednK // ltn_subRL addn0 incremental_Nfull_strict_bounded //.
Qed.

(** We apply these two lemmas to prove that [iter_insert] satisfies
the specification. **)
Theorem iter_insert_has_spec vs t h :
   iter_insert_pre t h vs -> iter_insert_post vs t h (iter_insert vs t h).
Proof.
case=> IS H; rewrite /iter_insert_post.
move:(iter_insert_incremental vs IS)=>WS.
have LV: count_leaves (iter_insert vs t h) = count_leaves t + size vs.
- by rewrite !count_size (iter_insert_preorder vs IS) H size_cat //.
split=>// j; rewrite !get_preorderE /= LV (iter_insert_preorder vs IS).
rewrite H nth_cat -count_size.
  by case:(@ltnP j (count_leaves t))=>//= /ltn_addr -> //.
Qed.
End InternalAPI.

(** *** Canonical external API *)

(** Now we can define the canonical external API defined over
[IMT]s. *)

Section ExternalAPI.
Variable (h : nat).
Notation IMTTy := (@IMT commit_eqType hash_eqType h).
Implicit Type t : IMTTy.

(** [empty] IMTs are indeed compressed, as they are represented
internally by a [Empty] tree. *)
Program Definition empty : IMTTy := MakeIMT (size_of:=0) (incrementalE h) _.

(** We lift [get_root_height] to project the root commitment. *)
Definition get_root t : H := get_root_height (tree_of t) h.

(** We lift the model [get] function to define [get_cm_model].*)
Definition get_cm_model pos t := get pos h (tree_of t).

(** The specification of [get_cm_model] is straightforwards, it
projects the nth element of the preorder, as we have proven for
[get]. *)

Theorem get_cm_has_spec pos (t : IMTTy) :
  get_cm_model pos t =
     if pos < (size_of t) then Some (nth xo (preord (tree_of t)) pos)
     else None.
Proof.
by case:t=>/= t1 s1 I L; rewrite /get_cm_model /= -(eqP L) get_preorderE //.
Qed.

(** We pack [insert_model] to define [add_model]. We use Program to
infer the proof obligations of the Sigma type. *)

Program Definition add_model v t : IMTTy :=
  if (tree_of t) \in full then t
  else let t':= (insert_model v (tree_of t) h)
       in @MakeIMT _ _ h t'
                   (count_leaves t')
                   (insert_model_incremental v (imt_spec_incremental t))
                   _.

(** The post_condition of [add]: if we add an element [v] to a
non-full IMT [t], we obtain an IMT of the size [size_of t + 1], and
the inserted commitment [v] is placed in the next available
position. *)

Definition add_post (v:A) (t res:IMTTy) : Prop :=
  if (tree_of t) \in full then res = t
  else [/\ size_of res = (size_of t).+1 &
        get_cm_model (size_of t) res = Some v].

(** NB: [add_model] is total, so adding to a [full] IMT results in and
identity. *)

(** We break down the [add_model_has_spec] theorem into lemmas to
project the different conjuncts in the post-condition: *)

Lemma add_modelF (v : A) (t : IMTTy):
  (tree_of t) \in full -> add_model v t = t.
Proof. by case:t=>//= t1 s1 I1 L1 F; rewrite /add_model /= F/= //. Qed.

(** We prove an _unfolding_ lemma to show that the underlying
[tree_of] after adding a commitment to an IMT [t], is indeed computed
using [insert_model] of the interal API. *)
Lemma tree_of_add_model (v : A) (t : IMTTy):
  tree_of (add_model v t) =
   if (tree_of t) \in full then tree_of t else insert_model v (tree_of t) h.
Proof.
case:t=>/= t1 s1 I1 L1.
case:(fullP t1)=>[F|/negP /negbTE F]; first by rewrite add_modelF F //.
by rewrite /add_model /= F //.
Qed.

(** [add_model] indeed increses the size of [t] by (at most) one. *)
Lemma size_of_add_model (v : A) (t : IMTTy):
  size_of (add_model v t) =
  if (tree_of t) \in full then size_of t else (size_of t).+1.
Proof.
case:t=>/= t1 s1 I1 L1.
case:(fullP t1)=>[F|/negP F]; first by rewrite add_modelF F //.
rewrite /add_model /= (negbTE F) -[in RHS](eqP L1) //=.
suff: insert_model_pre t1 h by case/(insert_model_has_spec v).
by rewrite /insert_model_pre I1 F //.
Qed.

(** Algebraic consistency property of the exported API: by applying
[get_cm_model] after [add], either we get a previously existing value,
or the element [v] we just added to the IMT. *)
Lemma get_add_model pos (v : A) (t : IMTTy) :
  get_cm_model pos (add_model v t) =
  if (pos == size_of t) && ((tree_of t) \notin full) then Some v
  else get_cm_model pos t.
Proof.
case:t=>/= t1 s1 I1 L1.
case:(fullP t1)=>[F|/negP F]; first by rewrite add_modelF F // andbF //.
rewrite F andbT //= 2!get_cm_has_spec size_of_add_model /= (negbTE F) /=.
rewrite /add_model //= (negbTE F) /= (insert_model_preord _ I1 F).
rewrite -(eqP L1) nth_rcons ltnS /= leq_eqVlt -count_size.
case:eqP=>//= [->|_]; first by rewrite ltnn //.
by case:ifP=>//->//.
Qed.

(** Notice that the only pre-condition for [add_model] is that [t] has
the right type (and it is therefore an [incremental] tree), as the
function is total. Thus the specification theorem is reduced to just
asserting the post_condition. *)
Theorem add_model_has_spec (v:A) (t: IMTTy): add_post v t (add_model v t).
Proof.
case:t=>/= t1 s1 I1 S1.
rewrite /add_post /= size_of_add_model get_add_model /=.
case:(fullP t1)=>[F|/negP F]; first by rewrite add_modelF F //.
by rewrite (negbTE F) eq_refl //.
Qed.

(** Now we pack the external API for batched iterated addition to an
IMT: if we have enough space we insert the whole batch, otherwise the
IMT remains the same. *)

Program Definition iter_add_model vs t : IMTTy :=
  let (t0,_,IT,_) := t
  in if 2^h < count_leaves t0 + size vs then t
     else let t':= (iter_insert vs t0 h)
          in @MakeIMT _ _ h t' (count_leaves t')
                      (iter_insert_incremental vs IT) _.

(** The post_condition of [iter_add] lifts the post-condition of
[iter_insert_model] iteratively: (i) if we add [vs] elements to a
non-full IMT [t] with enough space we obtain an IMT of size
[count_leaves t + size_of vs], and (ii) the algebraic property about
the position of the commitments in the preorder. *)

Definition iter_add_post (vs:seq A) (t0 res:IMTTy) : Prop :=
 if 2 ^ h < count_leaves (tree_of t0) + size vs then res = t0
 else [ /\ count_leaves (tree_of res) = count_leaves (tree_of t0) + size vs &
        forall j, get j h (tree_of res) =
               if count_leaves (tree_of t0) <= j
                  < count_leaves (tree_of t0) + size vs
               then Some (nth xo vs (j - count_leaves (tree_of t0)))
               else get j h (tree_of t0)].

(** Finally, we prove that iterated insertion satisfies the
specification: *)
Theorem iter_add_has_spec (vs: seq A) (t: IMTTy):
  iter_add_post vs t (iter_add_model vs t).
Proof.
case:t=>/= t1 s1 I1 S1; rewrite /iter_add_post; case:ltnP=>//= P.
have:iter_insert_pre t1 h vs by rewrite /iter_insert_pre P I1 //.
by case/iter_insert_has_spec => IS CL GH; split=>//.
Qed.
End ExternalAPI.
End Canonical.
End IncrementalTrees.
Export IncrementalTrees.
