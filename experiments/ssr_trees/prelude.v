(* The MIT License (MIT)

 Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. *)

From mathcomp.ssreflect Require Import ssreflect ssrbool eqtype.

(** Some Ltac for dealing with lifted eqP axioms in eqP instances. We
should do better than this, but it avoids tedious boilerplate. *)

Export Set Implicit Arguments.
Export Unset Strict Implicit.

Ltac eqP_automator_header H1 :=
intros x y; case:x; intros; case:y; intros; rewrite /H1 //=.

Ltac eqP_automator_records :=
repeat (case:eqP=>[->|H];
 rewrite ?andFb ?andbT //; try by constructor; case=>/H//=);
last by constructor.

Ltac eqP_automator_inductives :=
  try (by constructor);
    repeat (case: eqP=>//[->|NE];
               rewrite ?andTb ?andbT;
               try constructor;
               [ idtac | by case=>/NE//]).

Ltac eqP_automator H1 :=
eqP_automator_header H1;
try (eqP_automator_records || eqP_automator_inductives).


Notation predI3 x y z := (predI (predI x y) z).
