(* The MIT License (MIT)

 Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. *)

From mathcomp.ssreflect
     Require Import ssreflect ssrbool ssrnat eqtype ssrfun seq.

From IMT
     Require Import prelude commitments imt_model imt_position.

Export Set Implicit Arguments.
Export Unset Strict Implicit.

(** The first implementation we verify, optimizes the fact we always
insert in the next available position, so we need not verify wether
the tree is full. Instead, we rather check whether the position
argument (tied to the count of leaves in the IMT by the
pre-condition), entails whether we need to navigate on the left or
right subtree of insert.*)

(** We have two distinct implementations of this optimization: one
called [insert_pos], which is defined in insert_pos.v, where the
[pos]ition argument is re-computed when navigating the right-subtree;
and the one we verify in this buffer, called [insert_pow], where the
[pos] argument is fixed, and so it requires reasoning with overflowing
[pos] when it becomes strictly bigger than the number of leaves in the
subtree. The latter one corresponds to the original implementation of
insert from the [sappling-integration branch]. The former one, is
inspired after the F* development, and the latter implementation of
insert in the Edo protocol.

Here we attempt to verify [insert_pow], the original implementation
where position is fixed, and thus require extra lemmas to reason about
the overflowing position argument. *)

(**

    let rec insert_overflow tree cm height pos =
      assert (Compare.Int64.(pos >= 0L && pos <= max_uint32)) ;
      assert (Compare.Int.(height >= 0 && height <= 32)) ;
      match tree with
      | Empty ->
         if Compare.Int.(height = 0) then Leaf cm
         else
           let t1 = insert_overflow Empty cm (height - 1) pos in
           let h = hash ~height t1 Empty
           in
           Node (h, t1, Empty)
      | Node (_, t1, t2) ->
         let left = go_left pos height in
         if left then
           let t1 = insert_overflow t1 cm (height - 1) pos in
           let h = hash ~height t1 t2
           in
           Node (h, t1, t2)
          else
            let t2 = insert_overflow t2 cm (height - 1) pos in
            let h = hash ~height t1 t2
            in
            Node (h, t1, t2)
      | Leaf _ ->
         (* assert (incremental t /\ t = Leaf w /\ ~ (full t)) *)
	 (* this is a contradiction under the precondition that we don't
            insert on full trees. *)
         assert false

*)

(* TODO: I was not able to finish the implementation with the
overflows. It is missing a lemma on [go_left_spec]. *)

Module InsertOverflow.
Export IncrementalTrees.
Notation A := commitments.Commitments.C.
Notation H := commitments.Commitments.H.
Notation tree := (@tree commit_eqType hash_eqType).
Notation xo := (hash_to_commitment (uncommitted 0)).

(** Internal API Implementation **)
Section InternalAPI.

(** The OCaml implementation of [go_left] uses bitwise arithmetic over
Int64 types, to compute the capacity of the sub-tree and to tests
whether the subtree is full. *)

(* let go_left pos height = *)
(*   let open Int64 in *)
(*   let mask = shift_left 1L (height - 1) in *)
(*   equal (logand pos mask) 0L *)

(** Instead, we define (and test) a recursive specification: *)

(*
let rec go_left_spec pos height =
    assert (Compare.Int64.(pos >=0L));
      if Compare.Int64.(pos = 0L) then true
      else let full = pow2 height
           in if Compare.Int64.(pos < full) then pos < pow2 (height - 1)
              else go_left_spec (Int64.sub pos full) height

*)

(** Notice that we cannot define [go_left_spec] as is in Coq, as its
definition is not structurally recursive: *)

Fail Fixpoint go_left_spec' (pos height: nat) :=
  if pos isn't S _ then true
  else if pos < 2 ^ height then pos < 2 ^ height.-1
       else go_left_spec' (pos - 2 ^ height) height.

(** So, again we will use a gas trick, as we can always have an upper
bound on the position. Notice also that in the case of an overflowing
position argument [pos], it will converge to 0 quicker than [gas]. *)

Fixpoint go_left_spec' (pos height gas: nat) :=
  if gas isn't S g then true
  else if pos < 2 ^ height then pos < 2 ^ height.-1
       else go_left_spec' (pos - 2 ^ height) height g.

Definition go_left_spec pos height := go_left_spec' pos height pos.+1.

Fixpoint insert_pow (v:A) (pos:nat) (t:tree) (h:nat) : tree :=
  match t with
       | Empty => if h isn't (S h') then Leaf v
                  else let t1 := insert_pow v pos Empty h'
                       in let w := merkle_hash h'
                               (get_root_height t1 h')
                               (get_root_height Empty h')
                          in Node w t1 Empty
       | Node cm tl tr =>
         (* We need the match on the declared height [h] argument to
         guide the termination checker *)
         if h isn't (S h') then Node cm tl tr
         else if go_left_spec pos h
              then let t1 := insert_pow v pos tl h'
                   in let w := merkle_hash h'
                                           (get_root_height t1 h')
                                           (get_root_height tr h')
                      in Node w t1 tr
              else let t1 := insert_pow v pos tr h'
                   in let w := merkle_hash h'
                                      (get_root_height tl h')
                                      (get_root_height t1 h')
                      in Node w tl t1
       | Leaf w =>
         (* not possible if the height precondition holds *)
          Leaf w
  end.

(** We will need to reason in terms of position overflows to prove the
 equivalence between [insert_pow], and the functional specification
 using [insert_model]. *)

(** The precondition of [insert_pow] is equivalent to the precondition
of [insert_pos], [insert_pos_pre], but we explicit mention the
boundaries on [p] rather than reflecting on the fullness of the tree
[t]. *)

Definition insert_pow_pre p (t: tree) h :=
  [/\ t \in incremental h, p = count_leaves t & p < 2 ^ h].

(** Inserting on an Empty tree ignores the position argument, and
triggers the process of building an initial tree. *)
Lemma insert_powE v p h :
    insert_pow v p Empty h = initial_tree v h.
Proof. by elim:h=>//= n <- //. Qed.

(** Inserting on [Leaf w] trees is discarded by the precondition *)
Lemma insert_powL v p w h : insert_pow v p (Leaf w) h = Leaf w.
Proof. by case:h=>//. Qed.

(** Some auxiliary lemmas about [go_left_spec] *)
Lemma go_specP0h p h : go_left_spec' 0 h p = true.
Proof. by case:p=>//= n; rewrite !expn_gt0 //. Qed.

Lemma go_specPp0 p x: go_left_spec' p 0 x = true.
Proof. by elim:x p=>//= x IH p; rewrite expn0; case:leqP=>//. Qed.

Lemma go_spec0h h : go_left_spec 0 h = true.
Proof. by rewrite /go_left_spec go_specP0h //. Qed.

Lemma go_specp0 p : go_left_spec p 0 = true.
Proof. by rewrite /go_left_spec go_specPp0 //. Qed.

(** We will prove the specification of [insert_pow] by showing that,
whenever the precondition holds, it is extensionally equivalent to the
canonical implementation, i.e. it results in the same trees, and thus
satisfies the same spec.

However, proving this fact in the [insert_pow_switch] lemma below
requires an auxiliay property of [insert_pow], where we show that when
the [pos] argument overflows the size of the tree, we will insert in
the last available position. The lemma [insert_pow_overflow], requires
an axiomatized property of [go_left]:
*)

Axiom axiom_go_left_spec : forall p  n,
       go_left_spec' p n.+1 (p + 2 ^ n.+1) =
               if p < 2 ^ n.+1 then p < 2 ^ n else
                 go_left_spec' (p - 2 ^ n.+1) n.+1 p.

Lemma insert_pow_overflow v p t n:
        insert_pow v (2 ^ n + p) t n = insert_pow v p t n.
Proof.
elim:n p t=>//= n IH p; case=>//=.
- by rewrite !insert_powE //.
move=>/= w l r.
rewrite /go_left_spec /= ltnNge leq_addr addnC /= -addnBA // subnn addn0.
rewrite (axiom_go_left_spec p n).
by case:ifP=>//_; rewrite expnS mul2n -addnn addnCA IH addnC IH //.
Admitted.

Lemma insert_pow_switch {v p t h}:
  insert_pow_pre p t h -> insert_pow v p t h = insert_model v t h.
Proof.
case=>IT E P.
rewrite /insert_model /=.
(* the precondition of insert_pow entails that of insert_model,
i.e. that t \notin full *)
rewrite E in P *; move:P.
rewrite (incremental_full_leaves IT)=>X.
rewrite (ltn_eqF X) /=.
elim:h t {E} IT X =>//n IH; case=>//=[|w l r IS].
- by rewrite insert_powE insert_modelE //.
case/andP:(incrementalN IS) => /= LI RI.
case:(fullP l)=>[F|/negP F]; rewrite ?F ?(negbTE F) /=; last first.
(* If [l \notin full] then [r] is [Empty], and we go to the left *)
- rewrite (incremental_NfullRE IS F) addn0 /= => X.
 move:(incremental_Nfull_strict_bounded LI F)=>Y.
 rewrite /go_left_spec /= Y X.
 by rewrite (IH _ LI Y).
move:(F);rewrite (incremental_full_leaves LI) =>/eqP X.
rewrite X /go_left_spec /=  {1 2}expnS mul2n -addnn ltn_add2l=> Y.
rewrite Y ltnNge leq_addr /= insert_pow_overflow //.
by rewrite (IH _ RI Y).
Qed.

Lemma insert_pow_has_spec v p t h :
  insert_pow_pre p t h -> insert_post v t h (insert_pow v p t h).
Proof.
move=>H; rewrite (insert_pow_switch H); apply:(insert_model_has_spec).
case:H=> IS -> P.
by rewrite /insert_model_pre IS (incremental_full_leaves IS) neq_ltn P //.
Qed.
End InternalAPI.

(** ** External API on [t : IMT h] *)

Section ExternalAPI.
Variable (h : nat).
Notation IMTTy := (@IMT commit_eqType hash_eqType h).
Implicit Type t : IMTTy.

(** We pack [insert_pow] to define [add_pow]. Again, we use Program to
infer the proof obligations of the Sigma type. Notice that if the
uderlying tree is [full], we don't apply [insert_pow] but rather just
return the full tree. *)
Program
 Definition add_pow v t : IMTTy :=
  let t':= if (tree_of t) \in full then (tree_of t)
           else (insert_pow v (size_of t) (tree_of t) h)
       in @MakeIMT _ _ h t' (count_leaves t') _ _.
Next Obligation.
case:t=>/= t1 s1 I1 /eqP/esym L1.
case:(fullP t1)=>[->//|/negP F]; rewrite (negbTE F).
(* We just need to prove that t' is an incremental Merkle tree. We
project this fact from the postcondition of [insert_pow]. *)
suff: insert_pow_pre s1 t1 h by case/(insert_pow_has_spec v).
by rewrite /insert_pow_pre I1 L1 incremental_Nfull_strict_bounded //.
Qed.

(** In order to prove the wrapped version correct wrt. the model
specification, we follow the same strategy we did for the underlying
trees and will just show that [add_model] and [add_pos] generate the
same IMTs: *)
Lemma add_pow_switch v t : add_pow v t = add_model v t.
Proof.
case:t=>/= t1 s1 I1 L1.
rewrite /add_pow /add_model //=.
(* Again, we need to do some convoying to deal with the hidden
dependencies in the proofs *)
set cp := count_leaves _.
set tp := if t1 \in full then t1 else insert_pow v s1 t1 h.
set I2 := add_pow_obligation_1 v _.
set I3 := insert_model_incremental v _.
set L2 := add_pow_obligation_2 v _.
set L3 := imt_model.IncrementalTrees.add_model_obligation_1 v _.
move:(eqP L1)=>L; move:L1 I2 I3 L2 L3; case:(fullP t1)=>[F|/negP F].
- rewrite /insert_model F // -L in cp tp * => L1 I2 I3 L2 L3.
 by rewrite (bool_irrelevance I1 I2) (bool_irrelevance L1 L2) //.
(* A bit of forward thinking to be able to rewrite with the
[insert_pow_switch] lemma on [tp] and it lemmas. *)
have P: insert_pow_pre s1 t1 h.
by rewrite /insert_pow_pre I1 - L incremental_Nfull_strict_bounded //.
rewrite (negbTE F) //= (insert_pow_switch P) in cp tp * => L1 I2 I3 L2 L3.
by rewrite (bool_irrelevance I2 I3) (bool_irrelevance L2 L3) //.
Qed.

(** The fact that [add_pow] satisifes the specification follows
trivially from the previous lemma: *)
Theorem add_pow_has_spec (v:A) (t: IMTTy): add_post v t (add_pow v t).
Proof. by rewrite add_pow_switch; exact:(add_model_has_spec). Qed.

(** This optimization does not change the implementation of
[get]. However, for completness sake we just give [get_cm_pow], and
the proof it satisfies its specification. *)

Definition get_cm_pow := get_cm_pos.

Theorem get_cm_pow_has_spec pow t:
  get_cm_pow pow t =
     if pow < (size_of t) then Some (nth xo (preord (tree_of t)) pow)
     else None.
Proof. rewrite /get_cm_pow get_cm_pos_has_spec //. Qed.
End ExternalAPI.
End InsertOverflow.
Export InsertOverflow.
