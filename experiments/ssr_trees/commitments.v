(* The MIT License (MIT)

 Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. *)

From mathcomp.ssreflect
     Require Import ssreflect ssrbool ssrnat eqtype ssrfun seq.

From IMT Require Import prelude.

Export Set Implicit Arguments.
Export Unset Strict Implicit.

(** ** An axiomatic model of commitments *)

(** Pedersen's Commitment of a transaction output (i.e. address and
      value). A Merkle tree is formed with the existing commitment and
      filled with a default uncommitted value. **)
Module Commitments.
Context {C H: Set}.

(** In practice, we always deal with Merkle trees of fixed size (and
hence fixed height). We make this explicit by taking an extra
parameter fixing the size of the domain of the list of pre-defined
hashes [uncommitted]. *)
Parameter max_height : nat.

Notation hash_dom := ((fun j => j <= max_height) : pred nat).

Axiom axiom_commit_eqMixin : Equality.mixin_of C.

Canonical Structure commit_eqType :=
  Eval hnf in EqType C axiom_commit_eqMixin.

Axiom axiom_hash_eqMixin : Equality.mixin_of H.

Canonical Structure hash_eqType :=
  Eval hnf in EqType H axiom_hash_eqMixin.

(** Default uncommitted hashes indexed by height. *)
Parameter uncommitted : nat -> H.

(** Hash function to compute the Merkle tree (Pedersen's hash on
JubJub curve). Height is the height we are hashing at in the Merkle
tree. **)
Parameter merkle_hash : nat -> H -> H -> H.

(** We assume perfect (injective) hashing for each valid level: *)
Axiom perfect_merkle :
       {in hash_dom, forall h,  injective (merkle_hash h)}.

(** Hashes and commitments are the same object but are given different
      types to avoid confusing nodes and leaves. **)
Parameter hash_of_commitment : C -> H.
Parameter hash_to_commitment : H -> C.

(** We reflect this fact by requiring that C and H are isomorphic: *)
Axiom hash_to_of_iso :
  (forall x, hash_to_commitment (hash_of_commitment x) = x)
   * (forall y, hash_of_commitment (hash_to_commitment y) = y).

(** We axiomatize the specification of the pre-computed default values
for each different height. That is, that each entry in the
[uncommitted] list corresponds to the root hash of a Merkle tree full
of [uncommitted 0] values at its leaves: *)
Axiom uncomitted_spec :
  { in hash_dom, forall h,
        uncommitted h.+1 = merkle_hash h (uncommitted h) (uncommitted h)}.

(** Notice that the behaviour is only defined for the right hash
heights. *)

End Commitments.
Export Commitments.
