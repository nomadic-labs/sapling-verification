(* The MIT License (MIT)

 Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. *)

From mathcomp.ssreflect
     Require Import ssreflect ssrbool ssrnat eqtype ssrfun seq.

From IMT
     Require Import prelude commitments imt_model.

Export Set Implicit Arguments.
Export Unset Strict Implicit.

(** * Incremental Merkle Trees implementations: fixed position insertion *)

(** The first implementation we verify optimizes the reference
implementation of insert given in imt_model.v by taking advantage of
the fact that we will always insert elements in the next available
position. Thus, given the [incremental] specification, we need not
compute whether the underlying subtree is [full]. Instead, we can
compare the position argument (that we will tie to the number of
commitments present in the IMT in its precondition) with the maximum
capacity of a subtree at the corresponding height. *)

(** We have two distinct implementations for this optimization:

 (i) one where where the [pos]ition argument is fixed, and thus
requires reasoning with overflowing [pos] when it becomes strictly
bigger than the number of leaves in a subtree;

 (ii) one where the [pos]ition argument is re-computed when navigating
the right-subtree.

The former one corresponds to the original implementation of insert in
the [sappling-integration branch] for the Edo protocol proopsal. We
here verify the latter, inspired after the F* specification effort,
and which can be defined in OCaml as follows: *)

(**

   let rec insert_pos tree cm height pos =
   assert (Compare.Int64.(pos >= 0L && pos <= max_uint32)) ;
   assert (Compare.Int.(height >= 0 && height <= 32)) ;
   match tree with
      | Empty ->
          if Compare.Int.(height = 0) then Leaf cm
          else
            let t1 = insert_pos Empty cm (height - 1) pos in
            let h = hash ~height t1 Empty
            in
            Node (h, t1, Empty)
      | Node (_, t1, t2) ->
         let full = pow2 height
         in if Compare.Int64.(pos < full) then
            let t1 = insert_pos t1 cm (height - 1) pos in
            let h = hash ~height t1 t2
            in
            Node (h, t1, t2)
          else
            let at = Int64.(sub full pos) in
            let t2 = insert_pos t2 cm (height - 1) at in
            let h = hash ~height t1 t2
            in
            Node (h, t1, t2)
      | Leaf _ ->
         (* assert (incremental t /\ t = Leaf w /\ ~ (full t)) *)
	 (* this is a contradiction under the precondition that we don't
            insert on full trees. *)
          assert false
*)

Module InsertPos.
Export IncrementalTrees.
Notation A := commitments.Commitments.C.
Notation H := commitments.Commitments.H.
Notation tree := (@tree commit_eqType hash_eqType).
Notation xo := (hash_to_commitment (uncommitted 0)).

(** ** Internal API* on incremental trees of height h *)

Section InternalAPI.
(** As we hinted above, we use the [pos] argument to avoid checking if
the left subtree is [full] or not in the [Node w l r] case. We will
use the fact that [pos] is bound by the pre-condition to the size of
the IMT to recompute the position argument on the right-subtree, and
thus avoid reasoning about the [pos] overflowing the size of the
subtree. *)

Fixpoint insert_pos (v:A) (pos:nat) (t:tree) (h:nat) : tree :=
  match t with
       | Empty => if h isn't (S h') then Leaf v
                  else let t1 := insert_pos v pos Empty h'
                       in let w := merkle_hash h'
                               (get_root_height t1 h')
                               (get_root_height Empty h')
                          in Node w t1 Empty
       | Node cm tl tr =>
         (* This is just to guide the termination checker *)
         if h isn't (S h') then Node cm tl tr
         else let full := 2 ^ h'
              in if pos < full
              then let t1 := insert_pos v pos tl h'
                   in let w := merkle_hash h'
                                           (get_root_height t1 h')
                                           (get_root_height tr h')
                      in Node w t1 tr
              else let t1 := insert_pos v (pos - full) tr h'
                   in let w := merkle_hash h'
                                      (get_root_height tl h')
                                      (get_root_height t1 h')
                      in Node w tl t1
       | Leaf w =>
         (* not possible if the height precondition holds *)
         Leaf w
  end.

(** The pre-condition of [insert_pos] states that we only insert
commitments into non-full trees, at the first available position
(i.e. the size of [t]). *)

Definition insert_pos_pre (t: tree) h p :=
  [/\ t \in incremental h, t \notin full & p = count_leaves t].

Lemma insert_posE v p h :
    insert_pos v p Empty h = initial_tree v h.
Proof. by elim:h=>//= n <- //. Qed.

(** If the precondition above holds, we show can that [insert_pos]
computes the same trees that the canonical implementation: *)
Lemma insert_pos_switch {v p t h}:
        insert_pos_pre t h p ->
             insert_pos v p t h = insert_model v t h.
Proof.
case=>T F E.
rewrite /insert_model /= (negbTE F) /=.
elim:h t p E T {F} =>//= n IH; case=>//[p _ _|].
- by rewrite insert_posE insert_modelE //.
move=>//= x l r p -> IS.
case/andP:(incrementalN IS)=>/= LI RI.
case:(fullP l)=>[F|/negP F]; rewrite ?F ?(negbTE F) /=.
(* If [l \in full], [count_leaves l = 2 ^ n], and we descend on the
right sub_tree. *)
- move:(F); rewrite (incremental_full_leaves LI) addnC =>/eqP ->.
 rewrite ltnNge leq_addl /= -addnBA // subnn addn0.
 (* It suffices to rewrite with IH *)
 by rewrite (IH r _ _ RI) //.
(* If [l \notin full] then [r] is [Empty] *)
rewrite (incremental_NfullRE IS F) addn0.
rewrite (incremental_Nfull_strict_bounded LI F) /=.
by rewrite (IH l _ _ LI) //.
Qed.

(** We prove that [insert_pos] satisfies the canonical specification:
it suffices to show that the precondition of [insert_pos], entails the
precondition of [insert_model] and also that they generate the same
trees by [insert_pos_switch] above. *)
Theorem insert_pos_has_spec v p t h :
     insert_pos_pre t h p ->
          insert_post v t h (insert_pos v p t h).
Proof.
move=>P; rewrite (insert_pos_switch P).
apply:insert_model_has_spec.
by case:P=> TH F _; rewrite /insert_model_pre TH F //.
Qed.



(** [get_cm_height] gets the commitmment at the [pos] position. **)
Fixpoint get_cm_height (pos height : nat) (t : tree) : option A :=
  match t with
  | Empty => None
  | Leaf v => if pos == 0 then Some v else None
  | Node w l r =>
    let full := 2 ^ height.-1
    in if pos < full then get_cm_height pos height.-1 l
    else get_cm_height (pos - full) height.-1 r
  end.

(** Again, we show that for any given tree, [get_pos] computes the
same values as the canonical [get]. *)
Lemma get_pos_switch p h t:
  t \in incremental h -> get_cm_height p h t = get p h t.
Proof.
elim:t p h=>//= v l IHL r IHR p h IS.
case/andP:(incrementalN IS)=>LI RI.
case:(fullP l)=>[F|/negP F]; rewrite ?F ?(negbTE F) /=.
(* If [l \in full], [count_leaves l = 2 ^ n], and we descend on the
right sub_tree. *)
- move:(F); rewrite (incremental_full_leaves LI) =>/eqP ->.
  by rewrite (IHL p _ LI) (IHR _ _ RI) //.
(* If [l \notin full] then [r] is [Empty] *)
rewrite (incremental_NfullRE IS F) [in RHS]ltnNge /=.
move: (incremental_Nfull_strict_bounded LI F)=>X.
case:ltnP=>[Y|]; last by move/(leq_trans (ltnW X))=>->//.
rewrite IHL // -ltnNge; case:ifP=>// P.
by rewrite get_preorderE P //.
Qed.
End InternalAPI.

(** ** External API on [t : IMT h] *)

(** In the same way we have done for the canonical implementation in
the model, we define the external API over the Incremental Trees
Sigma-type. *)

Section ExternalAPI.
Variable (h : nat).
Notation IMTTy := (@IMT commit_eqType hash_eqType h).
Implicit Type t : IMTTy.

(** We pack [insert_pos] to define [add_pos] over [t : IMT h]. Again,
we use Program to infer the proof obligations of the Sigma
type. Notice that if the uderlying tree is [full], we don't apply
[insert_pos] but rather just return the tree. *)
Program
 Definition add_pos v t : IMTTy :=
  let t':= if (tree_of t) \in full then (tree_of t)
           else (insert_pos v (size_of t) (tree_of t) h)
       in @MakeIMT _ _ h t' (count_leaves t') _ _.
Next Obligation.
case:t=>/= t1 s1 I1 /eqP/esym L1.
case:(fullP t1)=>[->//|/negP F]; rewrite (negbTE F).
(* We just need to prove that t' is an incremental Merkle tree. We
project this fact from the postcondition of [insert_pos]. *)
suff: insert_pos_pre t1 h s1 by case/(insert_pos_has_spec v).
by rewrite /insert_pos_pre F L1 I1 //.
Qed.

(** In order to prove the wrapped version correct wrt. the model
specification, we follow the same strategy we did for the underlying
trees and will just show that [add_model] and [add_pos] generate the
same IMTs: *)
Lemma add_pos_switch v t : add_pos v t = add_model v t.
Proof.
case:t=>/= t1 s1 I1 L1.
rewrite /add_pos /add_model //=.
(* A bit of convoying to deal with the hidden dependencies in the proofs *)
set cp := count_leaves _.
set tp := if t1 \in full then t1 else insert_pos v s1 t1 h.
set I2 := add_pos_obligation_1 v _.
set I3 := insert_model_incremental v _.
set L2 := add_pos_obligation_2 v _.
set L3 := imt_model.IncrementalTrees.add_model_obligation_1 v _.
move:(eqP L1)=>L; move:L1 I2 I3 L2 L3; case:(fullP t1)=>[F|/negP F].
- rewrite /insert_model F // -L in cp tp * => L1 I2 I3 L2 L3.
 by rewrite (bool_irrelevance I1 I2) (bool_irrelevance L1 L2) //.
rewrite (negbTE F) //= insert_pos_switch // L in cp tp * => L1 I2 I3 L2 L3.
by rewrite (bool_irrelevance I2 I3) (bool_irrelevance L2 L3) //.
Qed.

(** It suffices to apply the lemma above to conclude that [add_pos]
satisifes the implementation: *)
Theorem add_pos_has_spec (v:A) (t: IMTTy): add_post v t (add_pos v t).
Proof. by rewrite add_pos_switch; exact:(add_model_has_spec). Qed.

(** In the same way, we will wrap [get_cm_height] to define
[get_cm_pos], and again prove it satisfies the specification of get by
interchange with [get_cm_switch]. *)

Definition get_cm_pos pos t := get_cm_height pos h (tree_of t).

Lemma get_cm_pos_switch pos t : get_cm_pos pos t = get_cm_model pos t.
Proof.
by case:t=> t1 s1 I1 L1; rewrite /get_cm_pos /get_cm_model get_pos_switch //.
Qed.

Theorem get_cm_pos_has_spec pos t:
  get_cm_pos pos t =
     if pos < (size_of t) then Some (nth xo (preord (tree_of t)) pos)
     else None.
Proof. by rewrite get_cm_pos_switch get_cm_has_spec //. Qed.
End ExternalAPI.
End InsertPos.
Export InsertPos.
