
Inductive tree {A:Set} : Set :=
  | Empty : tree
  | Leaf : forall (v:A), tree
  | Node : forall (v:A), tree -> tree -> tree.

Definition t0 := Empty (A:=nat).
Definition t1 := Node 1 (Node 2 (Leaf 4) Empty) Empty.
Definition t2 := Node 1 (Node 2 (Leaf 4) (Leaf 5)) Empty.
Definition t3 := Node 1 (Node 2 (Leaf 4) (Leaf 5)) (Node 3 (Leaf 6) Empty).
Definition t4 := Node 1 (Node 2 (Leaf 4) (Leaf 5)) (Node 3 (Leaf 6) (Leaf 7)).

Fixpoint full {A:Set} (t:tree (A:=A)) : bool :=
  match t with
  | Empty => false
  | Leaf _ => true
  | Node _ l r => full l && full r
  end.

Example ex0: full t0 = false. Proof. auto. Qed.
Example ex4: full t4 = true. Proof. auto. Qed.

(* proofs are harder in functional *)
Theorem full_children : forall (A:Set) (v:A) (l:tree) (r:tree),
    full (Node v l r) (A:=A) = true -> full l = true.
Proof.
Admitted.

Fixpoint full_prop {A:Set} (t:tree (A:=A)) : Prop :=
  match t with
  | Empty => False
  | Leaf _ => True
  | Node _ l r => full_prop l /\ full_prop r
  end.

(* computation is harder in relational *)
Example ex0p: full_prop t0 = False. Proof. auto. Qed.
Example ex4p: full_prop t4 = True. Proof. simpl. Admitted.

Theorem full_prop_children : forall (A:Set) (v:A) (l:tree) (r:tree),
    full_prop (Node v l r) (A:=A) -> full_prop l /\ full_prop r.
Proof. intros. simpl in H. destruct H. auto. Qed.

Fixpoint incremental {A:Set} (t:tree (A:=A)) : bool :=
  match t with
  | Empty => true
  | Leaf _ => true
  | Node _ l Empty => incremental l
  | Node _ l r => full l && incremental r
  end.

Example ex0a: incremental t0 = true. Proof. auto. Qed.
Example ex4a: incremental t4 = true. Proof. auto. Qed.

Definition t1_wrong := Node 1 (Node 2 Empty (Leaf 4)) Empty.
Definition t2_wrong := Node 1 Empty (Node 2 (Leaf 4) (Leaf 5)).
Definition t3_wrong := Node 1 (Node 2 (Leaf 4) (Leaf 5)) (Node 3 Empty (Leaf 6)).
Definition t3_wrong2 := Node 1 (Node 3 (Leaf 6) Empty) (Node 2 (Leaf 4) (Leaf 5)).

Example ex1w: incremental t1_wrong = false. Proof. auto. Qed.
Example ex2w: incremental t2_wrong = false. Proof. auto. Qed.
Example ex3w: incremental t3_wrong = false. Proof. auto. Qed.
Example ex3w2: incremental t3_wrong2 = false. Proof. auto. Qed.

Fixpoint incremental_prop {A:Set} (t:tree (A:=A)) : Prop :=
  match t with
  | Empty => True
  | Leaf _ => True
  | Node _ l Empty => incremental_prop l
  | Node _ l r => full_prop l /\ incremental_prop r
  end.

Theorem full_is_incremental : forall (A:Set) (t:tree),
    full_prop t -> incremental_prop t (A:=A).
Proof. intros. induction t.
- simpl. auto.
- simpl. auto.
- simpl. destruct t6.
  + apply full_prop_children in H. destruct H. auto.
  + split.
    apply full_prop_children in H. destruct H. auto.
    apply IHt2. simpl. auto.
  + split. apply full_prop_children in H. destruct H. auto. apply IHt2.
    apply full_prop_children in H. destruct H. auto.
Qed.

(* insert element v at pos in tree t of height h *)
Fixpoint insert {A:Set} (v:A) (pos:nat) (t:tree) (h:nat): tree :=
  match t with
  | Empty =>
    match h with
    | 0 => Leaf v
    | S h' =>
      let t1 := insert v pos Empty h' in
      Node v t1 Empty
    end
  | Node _ t1 t2 =>
    match h with
    | 0 => Empty (* should not be possible *)
    | S h' =>
      match full t1 with
      | true =>
        let t1 := insert v pos t1 h' in
        Node v t1 t2
      | false =>
        let t2 := insert v pos t2 h' in
        Node v t1 t2
      end
    end
  | Leaf _ => t (* should not be possible *)
  end.

Definition preserves_incremental : forall (A:Set) (t:tree) (t':tree) (v:A) (pos:nat) (h:nat),
    incremental_prop t -> insert v pos t h = t' -> incremental_prop t'.
Admitted.
