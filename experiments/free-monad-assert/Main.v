Require Import Psatz.

(** This toy monad does nothing in term of computations. The [Assert] operator
    allows to introduce a proposition which we expect to be true. This could
    also be a boolean value instead of a proposition. *)
Module Monad.
  Inductive t (A : Type) : Type :=
  | Return : A -> t A
  | Bind : forall {B : Type}, t B -> (B -> t A) -> t A
  | Assert : Prop -> A -> t A.
  Arguments Return {_}.
  Arguments Bind {_ _}.
  Arguments Assert {_}.
End Monad.

(** A run is a witness showing that we are able to execute a monadic expression
    to a certain value. *)
Module Run.
  Inductive t {A : Type} (v : A) : Monad.t A -> Prop :=
  | Return : t v (Monad.Return v)
  | Bind : forall x v_x f, t v_x x -> t v (f v_x) -> t v (Monad.Bind x f)
  | Assert : forall (P : Prop), P -> t v (Monad.Assert P v).
End Run.

(** Some examples using the assert monad. *)
Definition f (n : nat) : Monad.t nat :=
  Monad.Assert (n > 12) (1 + n).

Definition g (n : nat) : Monad.t nat :=
  Monad.Bind (Monad.Return (2 + n)) (fun n => f n).

(** We can execute [g] with the hypothesis that [n] is greater than [10]. This
    proof would not work with [n] greater than [9] for example. *)
Lemma g_run (n : nat) (H : n > 10) : Run.t (3 + n) (g n).
  apply Run.Bind with (v_x := 2 + n).
  - apply Run.Return.
  - apply Run.Assert.
    lia.
Qed.
